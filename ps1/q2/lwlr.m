function y = lwlr(X_train, y_train, x, tau)

%% Initialization
lambda = 0.0001;
m = size(X_train, 1);
X_train = [ones(m, 1) X_train];

%% Compute Weights
w = ones(m, 1);
for i = 1:m
    w(i) = exp(-sum((X_train(i, :) - [1 x']).^2) / 2 / tau^2);
end

%% Minimize Cost Function with Newton's Method
theta = zeros(size(X_train, 2), 1);
for i = 1:10
    hTheta = sigmoid(X_train * theta);
    
    z = w .* (y_train - hTheta);
    grad = X_train' * z - lambda * theta;
    
    D = diag(- w .* (hTheta .* (1 - hTheta)) - lambda);
    H = X_train' * D * X_train;
    
    theta = theta - H \ grad;
end

%% Return Trained Results
y = ([1 x'] * theta > 0);

end


%% Logistic Function
function g = sigmoid(z)
g = 1 ./ (1 + exp(-z));
end
