function theta = l1ls(X, y, lambda)

n = size(X, 2);
theta = zeros(n, 1);
theta_prev = ones(n, 1);

while (norm(theta_prev - theta) > 1e-5)
    theta_prev = theta;
    for i = 1:n
        
        theta(i) = 0;
        theta_pos = - (X(:,i)'*(X*theta - y) + lambda) / (X(:,i)'*X(:,i));
        theta_pos = max(theta_pos, 0);
        theta(i) = theta_pos;
        J_pos = 0.5*norm(X*theta - y, 2)^2 + lambda*norm(theta, 1);

        theta(i) = 0;
        theta_neg = - (X(:,i)'*(X*theta - y) - lambda) / (X(:,i)'*X(:,i));
        theta_neg = min(theta_neg, 0);
        theta(i) = theta_neg;
        J_neg = 0.5*norm(X*theta - y, 2)^2 + lambda*norm(theta, 1);

        if (J_pos <= J_neg)
            theta(i) = theta_pos;
        else
            theta(i) = theta_neg;
        end

    end
end

end
