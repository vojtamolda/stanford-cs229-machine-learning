function centroids = compute_centroids(X, clusters, k)

n = size(X, 2);
centroids = zeros(k, n);

for ik = 1:k
   centroids(ik,:) = mean(X((clusters == ik),:));
end

end
