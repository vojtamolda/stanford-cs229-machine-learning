function clusters = compute_clusters(X, centroids, k)

m = size(X, 1);
distances = zeros(k, m);

for im = 1:m
    for ik = 1:k
        delta = (centroids(ik,:) - X(im,:))';
        distances(ik, im) = delta' * delta;
    end
end
[~, clusters] = min(distances);

end
