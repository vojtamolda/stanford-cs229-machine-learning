function [clusters, centroids] = k_means(X, k)

% Randomly select datapoints as centroids
centroids = init_centroids(X, k);
clusters = compute_clusters(X, centroids, k);

% Iteratively move centroids to the new mean values each cluster
for i = 1:100
    centroids = compute_centroids(X, clusters, k);
    clusters = compute_clusters(X, centroids, k);
    draw_clusters(X, clusters, centroids);
end

end
