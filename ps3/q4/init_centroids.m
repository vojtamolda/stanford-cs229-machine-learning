function centroids = init_centroids(X, k)

randidx = randperm(size(X, 1));
centroids = X(randidx(1:k), :);

end
