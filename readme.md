# Stanford Course CS229 - Machine Learning (Spring 2016)


Lecture videos, exams, assignments and other materials can be downloaded from the [course webpage](https://see.stanford.edu/Course/CS229).


## Prerequisites
- Knowledge of basic computer science principles and skills, at a level sufficient to write a reasonably
  non-trivial computer program.
- Familiarity with the basic probability theory
- Familiarity with the basic linear algebra


## Course Description

This course provides a broad introduction to machine learning and statistical pattern recognition.

Topics include: supervised learning (generative/discriminative learning, parametric/non-parametric learning, neural
networks, support vector machines); unsupervised learning (clustering, dimensionality reduction, kernel methods);
learning theory (bias/variance tradeoffs; VC theory; large margins); reinforcement learning and adaptive control.
The course will also discuss recent applications of machine learning, such as to robotic control, data mining,
autonomous navigation, bioinformatics, speech recognition, and text and web data processing.
