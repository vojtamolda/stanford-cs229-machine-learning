function [C, sigma] = dataset3Params(X, y, Xval, yval)
%EX6PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = EX6PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%

m = numel(y);
opts = [0.01 0.03 0.1 0.3 1 3 10 30];
accuracy = zeros(size(opts, 2), size(opts, 2));
precision = zeros(size(accuracy));
recall = zeros(size(accuracy));
factor = zeros(size(accuracy));

for i = 1:numel(opts)
    C = opts(i);
    for j = 1:numel(opts)
        sigma = opts(j);

        model = svmTrain(X, y, C, @(x1, x2) gaussianKernel(x1, x2, sigma));
        ypred = svmPredict(model, Xval);
        
        truePositives = sum((yval == 1) .* (ypred == 1));
        trueNegatives = sum((yval == 0) .* (ypred == 0));
        falsePositives = sum((yval == 1) .* (ypred == 0));
        falseNegatives = sum((yval == 0) .* (ypred == 1));
        
        accuracy(i, j) = (truePositives + trueNegatives) / m;
        precision(i, j) = truePositives / (truePositives + falsePositives);
        recall(i,j) = truePositives / (truePositives + falseNegatives);
        factor(i,j) = 2 * precision(i,j) * recall(i,j) / (precision(i,j) + recall(i,j));
    end
end

% figure; surf(opts, opts, accuracy);
% set(gca, 'XScale', 'log', 'YScale', 'log')
% title('Accuracy'); xlabel('C'); ylabel('sigma');
% 
% figure; surf(opts, opts, precision);
% set(gca, 'XScale', 'log', 'YScale', 'log')
% title('Precision'); xlabel('C'); ylabel('sigma');
% 
% figure; surf(opts, opts, recall);
% set(gca, 'XScale', 'log', 'YScale', 'log')
% title('Recall'); xlabel('C'); ylabel('sigma');
% 
% figure; surf(opts, opts, factor);
% set(gca, 'XScale', 'log', 'YScale', 'log')
% title('Factor'); xlabel('C'); ylabel('sigma');

[imax, jmax] = find(accuracy==(max(max(accuracy))));
C = opts(imax);
sigma = opts(jmax);

% =========================================================================

end
