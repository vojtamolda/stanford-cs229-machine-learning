# Machine Learning Online Class
#  Exercise 7 - Principal Component Analysis
#
#  Instructions
#  ------------
#
#  This file contains code that helps you get started on the exercise. You
#  will need to complete the following functions:
#
#     pca.py
#     project_data.py
#     recover_data.py
#
#  For this exercise, you will not need to change any code in this file, or
#  any other files other than those mentioned above.
#

import numpy as np
import scipy.io as scio
import scipy.misc as scmi
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d
from pca import pca
from recover_data import recover_data
from project_data import project_data
from display_data import display_data
from run_kmeans import run_kmeans
from feature_normalize import feature_normalize
from kmeans_init_centroids import kmeans_init_centroids
from plot_data_points import plot_data_points
from draw_line import draw_line


# ====================== Part 1: Load Example Dataset =========================
#  We start this exercise by using a small dataset that is easily to visualize.
#

print('Visualizing Example Dataset for PCA...')

# Load variable X from example dataset that we will be using
data = scio.loadmat('../ex7/ex7data1.mat')
X = data['X']

#  Visualize the example dataset
plt.plot(X[:, 0], X[:, 1], 'bo')
plt.axis([0.5, 6.5, 2, 8])
plt.axis('square')

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ====================== Part 2: Principal Component Analysis =================
#  You should now implement PCA, a dimension reduction technique. You should
# complete the code in pca.py
#

print('Running PCA on Example Dataset...')

#  Before running PCA, it is important to first normalize X
X_norm, mu, sigma = feature_normalize(X)

#  Run PCA
U, S = pca(X_norm)

#  Draw the eigenvectors centered at mean of data. These lines show the
#  directions of maximum variations in the dataset.
plt.hold(True)
plt.plot(X[:, 0], X[:, 1], 'bo')
draw_line(mu, mu + 1.5 * S[0] * U[:, [0]].T, '-k', linewidth=2)
draw_line(mu, mu + 1.5 * S[1] * U[:, [1]].T, '-k', linewidth=2)
plt.axis([0.5, 6.5, 2, 8])
plt.axis('square')
plt.hold(False)

print('Top eigenvector U[:, 0] = {}\n'.format(U[:, 0].T))
print('(you should expect to see [-0.707107 -0.707107])\n')

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ====================== Part 3: Dimension Reduction ==========================
#  You should now implement the projection step to map the data onto the first
#  k eigenvectors. The code will then plot the data in this reduced dimensional
#  space. This will show you what the data looks like when using only the
#  corresponding eigenvectors to reconstruct it.
#
#  You should complete the code in project_data.py
#

print('Dimension Reduction on Example Dataset...')

#  Plot the normalized dataset (returned from pca)
plt.plot(X_norm[:, 0], X_norm[:, 1], 'bo')
plt.axis([-4, 3, -4, 3])
plt.axis('square')

#  Project the data onto K = 1 dimension
K = 1
Z = project_data(X_norm, U, K)
print('Projection of the first example: {}'.format(float(Z[0])))
print('(this value should be about 1.481274)')

X_rec = recover_data(Z, U, K)
print('Approximation of the first example: {}'.format(X_rec[0, :]))
print('(this value should be about [-1.047419 -1.047419])\n')

#  Draw lines connecting the projected points to the original points
plt.hold(True)
plt.plot(X_rec[:, 0], X_rec[:, 1], 'ro')
for i in range(X_norm.shape[0]):
    draw_line(X_norm[i, :], X_rec[i, :], '--k', linewidth=1)
plt.hold(False)

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ================= Part 4: Loading and Visualizing Face Data =================
#  We start the exercise by first loading and visualizing the dataset. The
# following code will load the dataset into your environment
#

print('Loading Face Dataset...\n')

# Load face dataset
data = scio.loadmat('../ex7/ex7faces.mat')
X = data['X']

#  Display the first 100 faces in the dataset
display_data(X[:100, :])

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ================= Part 5: PCA on Face Data: Eigenfaces  =====================
#  Run PCA and visualize the eigenvectors which are in this case eigenfaces
#  We display the first 36 eigenfaces.
#

print('Running PCA on Face Dataset (this may take a minute)...\n')

#  Before running PCA, it is important to first normalize X
X_norm, mu, sigma = feature_normalize(X)

#  Run PCA
U, S = pca(X_norm)

#  Visualize the top 36 eigenvectors found
display_data(U[:, :36].T)

print('Program paused. Close the plot window to continue.\n')
plt.show()


# ================= Part 6: Dimension Reduction for Faces =====================
#  Project images to the eigen space using the top k eigenvectors. If you are
#  applying a machine learning algorithm.
#

print('Dimension Reduction for Face Dataset...')

K = 100
Z = project_data(X_norm, U, K)

print('The projected data Z has a size of: {}\n'.format(Z.size))

input('Program paused. Press enter to continue.\n')


# ======= Part 7: Visualization of Faces after PCA Dimension Reduction ========
#  Project images to the eigen space using the top K eigen vectors and
#  visualize only using those K dimensions Compare to the original input,
#  which is also displayed

print('Visualizing the Projected (Reduced Dimension) Faces...\n')

K = 100
X_rec = recover_data(Z, U, K)

# Display normalized data
plt.subplot(1, 2, 1)
display_data(X_norm[:100, :])
plt.title('Original Faces')

# Display reconstructed data from only k eigenfaces
plt.subplot(1, 2, 2)
display_data(X_rec[:100, :])
plt.title('Recovered Faces')

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ======= Part 8(a): Optional (ungraded) Exercise: PCA for Visualization ======
#  One useful application of PCA is to use it to visualize high-dimensional
#  data. In the last K-Means exercise you ran K-Means on 3-dimensional pixel
#  colors of an image. We first visualize this output in 3D, and then
#  apply PCA to obtain a visualization in 2D.
#

print("Plotting 3D Image RGB Dataset...\n")

# Load image
A = scmi.imread('../ex7/bird_small.png', mode='RGB') / 255
img_size = A.shape
X = A.reshape([img_size[0] * img_size[1], 3])
K, max_iters = 16, 10

# Run K-Means
initial_centroids = kmeans_init_centroids(X, K)
centroids, idx = run_kmeans(X, initial_centroids, max_iters)

# Sample 1000 random indexes (since working with all the data is too expensive.
# If you have a fast computer, you may increase this.
sel = np.random.random_integers(0, X.shape[0], 1000)

#  Visualize the data and centroid memberships in 3D'
ax = plt.figure().gca(projection='3d')
ax.scatter(X[sel, 0] * 255, X[sel, 1] * 255, X[sel, 2] * 255, c=idx[:, sel])
plt.xlabel('R'); plt.ylabel('G')
plt.title('Pixel Dataset Plotted in 3D. Color Shows Centroid Memberships')

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ======= Part 8(b): Optional (ungraded) Exercise: PCA for Visualization ======
# Use PCA to project this cloud to 2D for visualization.
#

print("Reducing Image Dataset to 2D...\n")

# Subtract the mean to use PCA
[X_norm, mu, sigma] = feature_normalize(X)

# PCA and project the data to 2D
U, S = pca(X_norm)
Z = project_data(X_norm, U, 2)

# Plot in 2D
plt.figure()
plot_data_points(Z[sel, :], idx[:, sel], K)
plt.title('Pixel Dataset Plotted in 2D. PCA Reduced Dimensionality')

print('Program paused. Close the plot window to continue.\n')
plt.show()
