import numpy as np


def project_data(X, U, K):
    """Computes the reduced data representation when projecting only on to the
     top k eigenvectors. Projection of the normalized inputs X into the
     reduced dimensional space spanned by the first K columns of U. It returns
     the projected examples.
    """

    # You need to return the following variables correctly.
    m, n = X.shape
    Z = np.zeros([m, K])

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Compute the projection of the data using only the top K
    #               eigenvectors in U (first K columns). For the i-th example
    #               X[i, :], the projection on to the k-th eigenvector is given
    #               as follows:
    #                    x = X[i, :]
    #                    projection_k = x.T * U[:, k]
    #

    Ured = U[:, :K]
    Z = X @ Ured

    # =========================================================================

    return Z
