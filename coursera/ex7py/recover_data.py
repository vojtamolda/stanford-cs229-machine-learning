import numpy as np


def recover_data(Z, U, K):
    """Recovers an approximation of the original data when using the projected
     data. Aproximate the original data that has been reduced to K dimensions.
     It returns the pproximate reconstruction in X_rec.
     """

    # You need to return the following variables correctly.
    X_rec = np.zeros([Z.shape[0], U.shape[0]])

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Compute the approximation of the data by projecting back
    #               onto the original space using the top K eigenvectors in U.
    #
    #               For the i-th example Z[i, :], the (approximate) recovered
    #               data for dimension j is given as follows:
    #                     v = Z[i, :]
    #                    recovered_j = v @ U[j, :K].T
    #
    #               Notice that U[j, :K] is a row vector.
    #

    Ured = U[:, :K]
    X_rec = Z @ Ured.T

    # =========================================================================

    return X_rec
