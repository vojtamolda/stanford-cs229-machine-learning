import matplotlib.pyplot as plt


def plot_data_points(X, idx, K):
    """Plots data points in X, coloring them so that those with the same index
     assignments in idx have the same color.
     """

    # Plot the data
    plt.scatter(X[:, 0], X[:, 1], c=idx, cmap='jet', alpha=0.9)
