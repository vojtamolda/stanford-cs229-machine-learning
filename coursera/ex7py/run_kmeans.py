import numpy as np
import matplotlib.pyplot as plt
from compute_centroids import compute_centroids
from find_closest_centroids import find_closest_centroids
from plot_data_points import plot_data_points
from draw_line import draw_line


def run_kmeans(X, initial_centroids, max_iters, plot_progress=False):
    """Runs the K-Means algorithm on data matrix X, where each row of X is a
     single example. It uses initial_centroids used as the initial centroids.
     max_iters specifies the total number of interactions of K-Means to 
     execute. plot_progress is a true/false flag that indicates if the function
     should also plot its progress as the learning happens. This is set to
     false by default. runkMeans returns centroids, a K x n matrix of the 
     computed centroids and idx, a m x 1 vector of centroid assignments
     (i.e. each entry in range [1..K]).
     """
    
    # Plot the data if we are plotting progress
    if plot_progress:
        plt.figure()
        plt.hold(True)
    
    # Initialize values
    m, n = X.shape
    K = initial_centroids.shape[0]
    centroids = initial_centroids
    previous_centroids = centroids
    idx = np.zeros([m, 1])
    
    # Run K-Means
    for i in range(max_iters):
        
        # Output progress
        print('\rK-Means {}/{}...'.format(i+1, max_iters), end='')
        
        # For each example in X, assign it to the closest centroid
        idx = find_closest_centroids(X, centroids)
        
        # Optionally, plot progress here
        if plot_progress:
            plot_progress_kmeans(X, centroids, previous_centroids, idx, K, i)
            previous_centroids = centroids

        # Given the memberships, compute new centroids
        centroids = compute_centroids(X, idx, K)
    
    # Hold off if we are plotting progress
    if plot_progress:
        plt.hold(False)

    print('\n')
    return centroids, idx


def plot_progress_kmeans(X, centroids, previous, idx, K, i):
    """Plots the data points with colors assigned to each centroid. With the 
     previous centroids, it also plots a line between the previous locations
     and current locations of the centroids.
     """
    
    # Plot the examples
    plot_data_points(X, idx, K)
    
    # Plot the centroids as black x's
    plt.plot(centroids[:, 0], centroids[:, 1], 'x',
             markeredgecolor='black', markersize=10, linewidth=3)
    
    # Plot the history of the centroids with lines
    for j in range(centroids.shape[0]):
        draw_line(centroids[j, :], previous[j, :], color='black')
    
    # Title
    plt.title('Iteration #{}'.format(i))
