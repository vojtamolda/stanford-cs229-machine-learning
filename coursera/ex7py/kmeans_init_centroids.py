import numpy as np


def kmeans_init_centroids(X, K):
    """This function initializes K centroids that are to be used in K-Means
     on the dataset X.
     """

    # ======================= YOUR CODE HERE ==================================
    # Instructions: You should set centroids to randomly chosen examples from
    #               the dataset X.
    #

    randidx = np.random.permutation(X.shape[0])
    centroids = X[randidx[1:K + 1], :]

    # =========================================================================

    return centroids
