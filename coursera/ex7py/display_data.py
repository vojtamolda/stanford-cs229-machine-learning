import math
import numpy as np
import matplotlib.pyplot as plt


def display_data(X, example_width=None):
    """Display 2D data 2D data stored in X in a nice grid. It returns the figure
      handle h and the displayed array if requested.
      """

    m, n = X.shape

    # Compute rows, cols
    if example_width is None:
        example_width = round(math.sqrt(n))
    example_height = n // example_width

    # Compute number of items to display
    display_rows = math.floor(math.sqrt(m))
    display_cols = math.ceil(m / display_rows)

    # Padding between images
    pad = 1

    # Setup blank display
    display_array = - np.ones([pad + display_rows * (example_height + pad),
                               pad + display_cols * (example_width + pad)])

    # Copy each example into a patch on the display array
    sample = 0
    for i in range(display_rows):
        for j in range(display_cols):
            if sample == m: break
            # Get the max value of the patch
            maxval = np.abs(X[sample, :]).max()

            # Copy the patch
            istart = pad + i * (example_width + pad)
            iend = istart + example_height
            jstart = pad + j * (example_height + pad)
            jend = jstart + example_height
            display_array[istart:iend, jstart:jend] = \
                X[sample, :].reshape([example_height, example_width]).T / maxval
            sample += 1

    # Display grayscale image without axis
    h = plt.imshow(display_array, cmap='gray', interpolation='nearest')
    plt.axis('off')

    return h, display_array
