# Machine Learning Online Class
#  Exercise 5 - Regularized Linear Regression and Bias-Variance
#
#  Instructions
#  ------------
# 
#  This file contains code that helps you get started on the exercise. You will
#  need to complete the following functions:
#
#     linear_reg_cost_function.py
#     learning_curve.py
#     validation_curve.py
#
#  For this exercise, you will not need to change any code in this file,
#  or any other files other than those mentioned above.
#

import numpy as np
import scipy.io as scio
import matplotlib.pyplot as plt
from linear_reg_cost_function import linear_reg_cost_function
from train_linear_reg import train_linear_reg
from learning_curve import learning_curve
from poly_features import poly_features
from feature_normalize import feature_normalize
from plot_fit import plot_fit
from validation_curve import validation_curve

# ================= Part 1: Loading and Visualizing Data ======================
#  We start the exercise by first loading and visualizing the dataset. The
#  following code will load the dataset into your environment and plot the data.
#

# Load Training Data
print('Loading and Visualizing Data...')

# Load X, y, Xval, yval, Xtest, ytest
data = scio.loadmat('../ex5/ex5data1.mat')
X, y = data['X'], data['y']
Xval, yval = data['Xval'], data['yval']
Xtest, ytest = data['Xtest'], data['ytest']

# Number of examples and features
m, n = X.shape

# Plot training data
plt.plot(X, y, 'rx', markersize=10, linewidth=1.5)
plt.xlabel('Change in water level (x)')
plt.ylabel('Water flowing out of the dam (y)')

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ================= Part 2: Regularized Linear Regression Cost ================
#  You should now implement the cost function for regularized linear regression.
#

theta = np.array([[1], [1]])
J, _ = linear_reg_cost_function(np.hstack([np.ones([m, 1]), X]), y, theta, 1)
print('Cost at theta = [1; 1]: {0:.6f}\n'
      '(this value should be about 303.993192)\n'.format(J))

input('Program paused. Press enter to continue.\n')

# ================= Part 3: Regularized Linear Regression Gradient ============
#  You should now implement the gradient for regularized linear regression.
#

theta = np.array([[1], [1]])
_, grad = linear_reg_cost_function(np.hstack([np.ones([m, 1]), X]), y, theta, 1)
print('Gradient at theta = [1; 1]: {}\n'
      '(this value should be about [-15.303016; 598.250744])\n'.format(grad.T))

input('Program paused. Press enter to continue.\n')

# ================= Part 4: Train Linear Regression ===========================
#  Once you have implemented the cost and gradient correctly, the train_
#  linear_reg function will use your cost function to train regularized linear
#  regression.
#
#  The data is non-linear, so this will not give a great fit.
#

#  Train linear regression with rlambda = 0
rlambda = 0
theta = train_linear_reg(np.hstack([np.ones([m, 1]), X]), y, rlambda)

#  Plot fit over the data
plt.plot(X, y, 'rx', markersize=10, linewidth=1.5)
plt.xlabel('Change in water level (x)')
plt.ylabel('Water flowing out of the dam (y)')
plt.hold(True)
plt.plot(X, np.hstack([np.ones([m, 1]), X]) @ theta, '--', linewidth=2)
plt.hold(False)

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ================= Part 5: Learning Curve for Linear Regression ==============
#  Next, you should implement the learning_curve function.
#
#  Since the model is underfitting the data, we expect to see a graph with
#  "high bias" -- slide 8 in 'lectures/advice.pdf'
#

rlambda = 0
err_train, err_valid = learning_curve(np.hstack([np.ones([m, 1]), X]), y,
                        np.hstack([np.ones([Xval.shape[0], 1]), Xval]), yval,
                        rlambda)

plt.plot(np.arange(m), err_train, np.arange(m), err_valid)
plt.title('Learning curve for linear regression')
plt.legend(['Train', 'Cross Validation'])
plt.xlabel('Number of training examples')
plt.ylabel('Error')
plt.axis([0, 13, 0, 150])

print('Linear Regression:')
print('# Training Examples\tTrain Error\tCross Validation Error')
for i in range(err_train.size):
    print('  \t{:d}\t\t{:.3f}\t{:.3f}'.
          format(i, float(err_train[i]), float(err_valid[i])))

print('\nProgram paused. Close the plot window to continue.\n')
plt.show()

# ================= Part 6: Feature Mapping for Polynomial Regression =========
#  One solution to this is to use polynomial regression. You should now
#  complete poly_features to map each example into its powers.
#

p = 8

# Map X onto Polynomial Features and Normalize
X_poly = poly_features(X, p)
X_poly, mu, sigma = feature_normalize(X_poly)
X_poly = np.hstack([np.ones([X_poly.shape[0], 1]), X_poly])

# Map X_poly_test and normalize (using mu and sigma)
X_poly_test = poly_features(Xtest, p)
X_poly_test = (X_poly_test - mu) / sigma
X_poly_test = np.hstack([np.ones([X_poly_test.shape[0], 1]), X_poly_test])

# Map X_poly_val and normalize (using mu and sigma)
X_poly_val = poly_features(Xval, p)
X_poly_val = (X_poly_val - mu) / sigma
X_poly_val = np.hstack([np.ones([X_poly_val.shape[0], 1]), X_poly_val])

print('Normalized Training Example 1: \n{}\n'.format(X_poly[[1], :]))

input('Program paused. Press enter to continue.\n')

# ================= Part 7: Learning Curve for Polynomial Regression ==========
#  Now, you will get to experiment with polynomial regression with multiple
#  values of lambda. The code below runs polynomial regression with 
#  lambda = 0. You should try running the code with different values of
#  lambda to see how the fit and learning curve change.
#

rlambda = 0
theta = train_linear_reg(X_poly, y, rlambda)

# Plot training data and fit
plt.figure(1)
plt.plot(X, y, 'rx', markersize=10, linewidth=1.5)
plot_fit(min(X), max(X), mu, sigma, theta, p)
plt.xlabel('Change in water level (x)')
plt.ylabel('Water flowing out of the dam (y)')
plt.title('Polynomial Regression Fit (lambda = {})'.format(rlambda))

plt.figure(2)
err_train, err_valid = learning_curve(X_poly, y, X_poly_val, yval, rlambda)
plt.plot(np.arange(m), err_train, np.arange(m), err_valid)

plt.title('Polynomial Regression Learning Curve (lambda = {})'.format(rlambda))
plt.xlabel('Number of training examples')
plt.ylabel('Error')
plt.axis([0, 13, 0, 100])
plt.legend(['Train', 'Cross Validation'])

print('Polynomial Regression (lambda = {}:)'.format(rlambda))
print('# Training Examples\tTrain Error\tCross Validation Error')
for i in range(err_train.size):
    print('  \t{:d}\t\t{:.3f}\t{:.3f}'.
          format(i, float(err_train[i]), float(err_valid[i])))

print('\nProgram paused. Close all plot windows to continue.\n')
plt.show()

# ================= Part 8: Validation for Selecting Lambda ===================
#  You will now implement validationCurve to test various values of lambda on
#  a validation set. You will then use this to select the "best" lambda value.
#

lambda_vec, err_train, err_valid = validation_curve(X_poly, y, X_poly_val, yval)

plt.plot(lambda_vec, err_train, lambda_vec, err_valid)
plt.legend(['Train', 'Cross Validation'])
plt.xlabel('lambda')
plt.ylabel('Error')

print('Lambda Selection:'.format(rlambda))
print('lambda\t\tTrain Error\tValidation Error')
for i in range(err_train.size):
    print('  \t{:.3f}\t\t{:.3f}\t{:.3f}'.
        format(float(lambda_vec[i]), float(err_train[i]), float(err_valid[i])))

print('\nProgram paused. Close the plot window to continue.\n')
plt.show()
