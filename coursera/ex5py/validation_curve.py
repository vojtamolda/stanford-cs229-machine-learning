import numpy as np
from train_linear_reg import train_linear_reg
from linear_reg_cost_function import linear_reg_cost_function


def validation_curve(X, y, Xval, yval):
    """ Returns the training and validation errors (in error_train, error_val)
     for different values of lambda. You are given the training set (X, y) and
     validation set (Xval, yval).
    """

    # Selected values of lambda (you should not change this)
    lambda_vec = np.array([[0, 0.001, 0.003, 0.01,
                            0.03, 0.1, 0.3, 1, 3, 10]]).T

    # You need to return these variables correctly.
    err_train = np.zeros(lambda_vec.shape)
    err_valid = np.zeros(lambda_vec.shape)

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Fill in this function to return training errors in
    #               err_train and the validation errors in err_val. The
    #               vector lambda_vec contains the different lambda parameters
    #               to use for each calculation of the errors, i.e,
    #               err_train[i], and err_valid[i] should give you the errors
    #               obtained after training with lambda = lambda_vec[i]
    #
    # Note: You can loop over lambda_vec with the following:
    #
    #       for i, rlambda in enumerate(lambda_vec):
    #           # Compute train / val errors when training linear regression
    #           # with regularization parameter rlambda. You should store the
    #           # result in err_train[i] and error_val(i)
    #           ...
    #

    for i, rlambda in enumerate(lambda_vec):
        theta = train_linear_reg(X, y, rlambda)
        err_train[i] = linear_reg_cost_function(X, y, theta, 0)[0]
        err_valid[i] = linear_reg_cost_function(Xval, yval, theta, 0)[0]

    # =========================================================================

    return lambda_vec, err_train, err_valid
