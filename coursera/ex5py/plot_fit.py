import numpy as np
import matplotlib.pyplot as plt
from poly_features import poly_features


def plot_fit(min_x, max_x, mu, sigma, theta, p):
    """Plots a learned polynomial regression fit over an existing figure. Also
     works with linear regression. Polynomial ower p and feature normalization
     (mu,sigma).
     """

    # Hold on to the current figure
    plt.hold(True)
    
    # We plot a range slightly bigger than the min and max values to get
    # an idea of how the fit will vary outside the range of the data points
    x = np.atleast_2d(np.arange(min_x - 15, max_x + 25, 0.05)).T
    
    # Map the X values 
    X_poly = poly_features(x, p)
    X_poly = (X_poly - mu) / sigma

    # Add a column of ones
    X_poly = np.hstack([np.ones(x.shape), X_poly])
    
    # Plot
    plt.plot(x, X_poly @ theta, '--', linewidth=2)
    
    # Hold off to the current figure
    plt.hold(False)
