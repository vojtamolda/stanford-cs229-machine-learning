import numpy as np
import scipy.optimize as spo
from linear_reg_cost_function import linear_reg_cost_function


def train_linear_reg(X, y, rlambda):
    """Trains linear regression using the dataset (X, y) and regularization
     parameter lambda. Returns the trained parameters theta.
     """

    # Initialize theta
    m, n = X.shape
    init_theta = np.zeros([n, 1])

    # Minimize using fmin_cg
    theta = spo.fmin_cg(
        f=lambda t, X, y, l:
                linear_reg_cost_function(X, y, t, l)[0],
        fprime=lambda t, X, y, l:
                linear_reg_cost_function(X, y, t, l)[1].ravel(),
        x0=init_theta, args=(X, y, rlambda), maxiter=200, disp=False)

    return theta
