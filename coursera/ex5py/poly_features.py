import numpy as np


def poly_features(X, p):
    """Takes a data matrix X (size m x 1) and maps each example into its
     polynomial features.

     X_poly[i, :] = [X[i] X[i]**2 X[i]**3 ...  X[i]**p]
    """

    # You need to return the following variables correctly.
    m, n = X.shape
    X_poly = np.zeros([m, p])

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Given a vector X, return a matrix X_poly where the p-th
    #               column of X contains the values of X to the p-th power.
    #

    for i in range(p):
        X_poly[:, [i]] = np.power(X, [i + 1])

    # =========================================================================

    return X_poly
