import numpy as np


def linear_reg_cost_function(X, y, theta, rlambda):
    """Computes the cost of using theta as the parameter for linear
     regression to fit the data points in X and y. Returns the cost in J and
     the gradient in grad.
    """

    # Initialize some useful variables
    m, n = X.shape
    theta = theta.reshape([n, 1])

    # You need to return the following variables correctly
    J = 0
    grad = np.zeros(theta.shape)

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Compute the cost and gradient of regularized linear
    #               regression for a particular choice of theta.
    #

    rtheta = np.vstack([[[0]], theta[1:]])

    J = ((X @ theta - y).T @ (X @ theta - y)
         + (rtheta.T @ rtheta) * rlambda) / 2 / m
    grad = (X.T @ (X @ theta - y) + rtheta * rlambda) / m

    # =========================================================================

    return float(J), grad
