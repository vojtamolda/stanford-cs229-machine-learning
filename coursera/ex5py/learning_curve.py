import numpy as np
from train_linear_reg import train_linear_reg
from linear_reg_cost_function import linear_reg_cost_function


def learning_curve(X, y, Xval, yval, rlambda):
    """Returns the train and cross validation set errors for a learning curve.
     In particular, it returns two vectors of the same length - err_train and
     err_val. Then, err_train[i] contains the training error for i examples
     (and similarly for err_val[i]).

     In this function, you will compute the train and test errors for
     dataset sizes from 1 up to m. In practice, when working with larger
     datasets, you might want to do this in larger intervals.
     """

    # Initialize some useful values
    m, n = X.shape

    # You need to return these values correctly
    err_train = np.zeros([m, 1])
    err_val = np.zeros([m, 1])

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Fill in this function to return training errors in
    #               error_train and the cross validation errors in error_val.
    #               i.e., err_train[i] and err_val[i] should give you the
    #               errors obtained after training on i examples.
    #
    # Note: You should evaluate the training error on the first i training
    #       examples (i.e., X[:i, :] and y[:i]).
    #
    #       For the cross-validation error, you should instead evaluate on
    #       the _entire_ cross validation set (Xval and yval).
    #
    # Note: If you are using your cost function (linear_reg_cost_function) to
    #       compute the training and cross validation error, you should call
    #       the function with the lambda argument set to 0. Do note that you
    #       will still need to use lambda when running the training to obtain
    #       the theta parameters.
    #
    # Hint: You can loop over the examples with the following:
    #
    #       for i in range(m):
    #           # Compute train/cross validation errors using training examples
    #           # X[:i, :] and y[:i], storing the result in err_train[i] and
    #           # err_val[i]
    #

    for i in range(1, m):
        theta = train_linear_reg(X[:i,:], y[:i], rlambda)
        err_train[i] = linear_reg_cost_function(X[:i, :], y[:i, :], theta, 0)[0]
        err_val[i] = linear_reg_cost_function(Xval, yval, theta, 0)[0]

    # =========================================================================

    return err_train, err_val
