import numpy as np
from sigmoid import sigmoid

def predict_one_vs_all(all_theta, X):
    """Returns a vector of predictions for each example in the matrix X. Note
     that X contains the examples in rows. all_theta is a matrix where the
     i-th row is a trained logistic regression theta vector for the i-th class.
     You should set p to a vector of values from 1..K (e.g., p = [1; 3; 1; 2]
     predicts classes 1, 3, 1, 2 for 4 examples)
    """

    # Add ones to the X data matrix
    m = X.shape[0]
    X = np.hstack([np.ones([m, 1]), X])

    # Some useful variables
    m, n = X.shape
    num_labels = all_theta.shape[0]

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Complete the following code to make predictions using your
    #               learned logistic regression parameters (one-vs-all). You
    #               should set p to a vector of predictions. Range is from 1 to
    #               num_labels.
    #
    # Hint: This code can be done all vectorized using the np.argmax function.
    #       In particular, the argmax function returns the index of the max
    #       element.
    #

    probabilities = sigmoid(X @ all_theta.T)
    p = np.argmax(probabilities, axis=1).reshape([m, 1]) + 1

    # =========================================================================

    return p
