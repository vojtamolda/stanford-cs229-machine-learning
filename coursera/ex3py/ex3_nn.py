# Machine Learning Online Class
#  Exercise 3: Neural Networks
#
#  Instructions
#  ------------
#
#  This file contains code that helps you get started on the linear exercise.
#  You will need to complete the following functions in this exericse:
#
#     predict.py
#
#  For this exercise, you will not need to change any code in this file, or any
#  other files other than those mentioned above.
#

import random
import numpy as np
import scipy.io as scio
import matplotlib.pyplot as plt
from display_data import display_data
from predict import predict

# Setup the parameters you will use for this part of the exercise
input_layer_size = 400  # 20x20 Input Images of Digits
hidden_layer_size = 25  # 25 hidden units
num_labels = 10  # Labels 1 to 10 (note that we have mapped "0" to label 10)

# ====================== Part 1: Loading and Visualizing Data =================
#  We start the exercise by first loading and visualizing the dataset.
#  You will be working with a dataset that contains handwritten digits.
#

# Load Training Data
print('Loading and Visualizing Data...\n')

data = scio.loadmat('../ex3/ex3data1.mat')
X, y = data['X'], data['y']
m, n = X.shape

# Randomly select 100 data points to display
rand_indices = [random.randint(0, m - 1) for _ in range(100)]
rand_selection = X[rand_indices[:100], :]
display_data(rand_selection)

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ====================== Part 2: Loading Parameters ===========================
#  In this part of the exercise, we load some pre-initialized neural network
#  parameters.
#

# Load the weights into variables Theta1 and Theta2
print('Loading Saved Neural Network Parameters...')

data = scio.loadmat('../ex3/ex3weights.mat')
Theta1, Theta2 = data['Theta1'], data['Theta2']

# ====================== Part 3: Implement Predict ============================
#  After training the neural network, we would like to use it to predict the
#  labels. You will now implement the "predict" function to use the neural
#  network to predict the labels of the training set. This lets you compute
#  the training set accuracy.
#

correct = np.where(predict(Theta1, Theta2, X) == y)
accuracy = float(correct[0].size / y.size * 100.0)
print('Training set accuracy: {0:.2f}%\n'.format(accuracy))

input('Program paused. Press enter to continue.\n')

#  To give you an idea of the network's output, you can also run
#  through the examples one at the a time to see what it is predicting.

for idx in [random.randint(0, m - 1) for _ in range(m)]:

    # Display random image
    display_data(X[[idx], :])

    # Calculate prediction
    digit = int(predict(Theta1, Theta2, X[[idx], :])) % 10
    plt.hold(True)
    plt.title('Neural Network Prediction: {}'.format(digit))
    plt.hold(False)

    print('Displaying example image. Close the plot window to continue.\n')
    plt.show(block=True)
