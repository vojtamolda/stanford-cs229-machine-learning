import numpy as np
from sigmoid import sigmoid


def lr_cost_function(theta, X, y, rlambda):
    """Computes the cost of using theta as the parameter for regularized
     logistic regression and the gradient of the cost w.r.t. to the parameters.
     """

    # Initialize some useful values
    m, n = X.shape
    theta = theta.reshape([n, 1])

    # You need to return the following variables correctly
    J = 0
    grad = np.zeros(theta.shape)

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Compute the cost of a particular choice of theta.You should
    #               set J to the cost. Compute the partial derivatives and set
    #               grad to the partial derivatives of the cost w.r.t. each
    #               parameter in theta
    #
    # Hint: The computation of the cost function and gradients can be
    #       efficiently vectorized. For example, consider the computation
    #
    #           sigmoid(X @ theta)
    #
    #       Each row of the resulting matrix will contain the value of the
    #       prediction for that example. You can make use of this to vectorize
    #       the cost function and gradient computations.
    #
    # Hint: When computing the gradient of the regularized cost function,
    #       there're many possible vectorized solutions, but one solution
    #       looks like:
    #
    #           grad = (unregularized gradient for logistic regression)
    #           temp = theta
    #           temp[1] = 0   # because we don't add anything for j = 0
    #           grad = grad + YOUR_CODE_HERE (using the temp variable)
    #

    h = sigmoid(X @ theta)
    J = - (y.T @ np.log(h) + (1 - y).T @ np.log(1 - h)) / m
    grad = X.T @ (h - y) / m

    rtheta = np.vstack([[[0]], theta[1:, :]])
    J += (rtheta.T @ rtheta) * rlambda / 2 / m
    grad += rtheta * rlambda / m

    # =========================================================================

    return float(J), grad
