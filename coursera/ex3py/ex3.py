# Machine Learning Online Class
#  Exercise 3: One-vs-All Logistic Regression
#
#  Instructions
#  ------------
# 
#  This file contains code that helps you get started on the linear exercise.
#  You will need to complete the following functions in this exericse:
#
#     lr_cost_function.py
#     one_vs_all.py
#     predict_one_vs_all.py
#
#  For this exercise, you will not need to change any code in this file, or any
#  other files other than those mentioned above.
#

import random
import numpy as np
import scipy.io as scio
import matplotlib.pyplot as plt
from display_data import display_data
from lr_cost_function import lr_cost_function
from one_vs_all import one_vs_all
from predict_one_vs_all import predict_one_vs_all

# Setup the parameters you will use for this part of the exercise
input_layer_size = 400  # 20x20 Input Images of Digits
num_labels = 10  # Labels 1 to 10 (note that we have mapped "0" to label 10)

# ====================== Part 1: Loading and Visualizing Data =================
#  We start the exercise by first loading and visualizing the dataset. 
#  You will be working with a dataset that contains handwritten digits.
#

# Load Training Data
print('Loading and Visualizing Data...\n')

data = scio.loadmat('../ex3/ex3data1.mat')
X, y = data['X'], data['y']
m, n = X.shape

# Randomly select 100 data points to display
rand_indices = [random.randint(0, m - 1) for _ in range(100)]
rand_selection = X[rand_indices[:100], :]
display_data(rand_selection)

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ====================== Part 2: Vectorize Logistic Regression ================
#  In this part of the exercise, you will reuse your logistic regression code
#  from the last exercise. You task here is to make sure that your regularized
#  logistic regression implementation is vectorized. After that, you will
#  implement one-vs-all classification for the handwritten digit dataset.
#

print('Training One-vs-All Logistic Regression...')

rlambda = 1
all_theta = one_vs_all(X, y, num_labels, rlambda)

input('\nProgram paused. Press enter to continue.\n')

# ====================== Part 3: Predict for One-Vs-All =======================
#  After training is completed predict the values over the dataset.
#

correct = np.where(predict_one_vs_all(all_theta, X) == y)
accuracy = float(correct[0].size / y.size)
print('Training set accuracy: {0:.2%}\n'.format(accuracy))
