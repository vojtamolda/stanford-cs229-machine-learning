import numpy as np
import scipy.optimize as spo
from lr_cost_function import lr_cost_function


def one_vs_all(X, y, num_labels, rlambda):
    """Trains num_labels logisitc regression classifiers and returns each of
     these classifiers in a matrix all_theta, where the i-th row of all_theta
     corresponds to the classifier for label i.
     """

    # Add ones to the X data matrix
    m = X.shape[0]
    X = np.hstack([np.ones([m, 1]), X])

    # Some useful variables
    m, n = X.shape

    # You need to return the following variables correctly
    all_theta = np.zeros([num_labels, n])

    # ====================== YOUR CODE HERE ======================
    # Instructions: You should complete the following code to train num_labels
    #               logistic regression classifiers with regularization
    #               parameter lambda.
    #
    # Hint: You can use np.where(y == c) to obtain a vector of 1's and 0's that
    #       tell use whether the ground truth is true/false for this class.
    #
    # Note: For this assignment, we recommend using fmin_cg to optimize the cost
    #       function. It is okay to use a for-loop (for c = 1:num_labels) to
    #       loop over the different classes.
    #
    #       fmin_cg works similarly to fmin_tnc, but is more efficient
    #       when we are dealing with large number of parameters.
    #
    # Example Code for fmin_cg:
    #
    #     init_theta = np.zeros([n, 1]);
    #     theta = scipy.optimize.fmin_cg(f=cost_func, fprime=grad_func
    #                                    x0=init_theta, args=(X, y))
    #

    init_theta = np.zeros([n, 1])
    for c in range(1, num_labels+1):
        print("\nLearning to identify {}:".format(c % 10))
        all_theta[c - 1, :] = \
            spo.fmin_cg(f=lambda t, X, y, l:
                            lr_cost_function(t, X, y, l)[0],
                         fprime=lambda t, X, y, l:
                            lr_cost_function(t, X, y, l)[1].ravel(),
                         x0=init_theta.ravel(),
                         args=(X, (y == c).astype(int), rlambda))

    # =========================================================================

    return all_theta
