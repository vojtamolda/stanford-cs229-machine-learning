import numpy as np
from sigmoid import sigmoid


def predict(Theta1, Theta2, X):
    """Predict the label of an input given the trained weights of a neural
     network (Theta1, Theta2)
     """

    # Useful values
    m, n = X.shape
    num_labels = Theta2.shape[1]

    # You need to return the following variables correctly
    p = np.zeros([m, 1])

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Complete the following code to make predictions using your
    #               learned neural network. You should set p to a vector
    #               containing labels between 1 to num_labels.
    #
    # Hint: This code can be done all vectorized using the np.argmax function.
    #       In particular, the argmax function returns the index of the max
    #       element.
    #

    a1 = np.hstack([np.ones([m, 1]), X])

    z2 = a1 @ Theta1.T
    a2 = np.hstack([np.ones([m, 1]), sigmoid(z2)])

    z3 = a2 @ Theta2.T
    a3 = sigmoid(z3)

    p = np.argmax(a3, axis=1).reshape([m, 1]) + 1

    # =========================================================================

    return p
