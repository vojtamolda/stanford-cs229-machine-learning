import numpy as np
import matplotlib.pyplot as plt
from plot_data import plot_data
from map_feature import map_feature


def plot_decision_boundary(theta, X, y):
    """Plots the data points with + for the positive examples and o for the
     negative examples. X is assumed to be a either
     1) Mx3 matrix, where the first column is an all-ones column for the intercept.
     2) MxN, N>3 matrix, where the first column is all-ones
     """

    plot_data(X[:, 1:3], y)

    plt.hold(True)
    if X.shape[1] <= 3:
        # Only need 2 points to define a line, so choose two endpoints
        plot_x = np.array([X[:, 2].min() - 2, X[:, 2].max() + 2])

        # Calculate the decision boundary line
        plot_y = (-1 / theta[2]) * (theta[1] * plot_x + theta[0])

        # Plot
        plt.plot(plot_x, plot_y)
    else:
        # Here is the grid range
        plot_x = np.linspace(-1, 1.5, 50)
        plot_y = np.linspace(-1, 1.5, 50)
        plot_x, plot_y = np.meshgrid(plot_x, plot_y)

        # Evaluate z = theta*x over the grid
        plot_z = np.zeros(plot_x.shape)
        for i in range(plot_z.shape[0]):
            for j in range(plot_z.shape[1]):
                plot_z[i, j] = map_feature(plot_x[i, j], plot_y[i, j]) @ theta

        # Plot single contour at 0.5
        plt.contour(plot_x, plot_y, plot_z, levels=[0.5], linewidth=2)
    plt.hold(False)
