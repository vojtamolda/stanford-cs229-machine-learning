# Machine Learning Online Class
#  Exercise 2: Logistic Regression
#
#  Instructions
#  ------------
# 
#  This file contains code that helps you get started on the logistic regression
#  exercise. You will need to complete the following functions in this exericse:
#
#     sigmoid.py
#     cost_function.py
#     predict.py
#     cost_function_reg.py
#
#  For this exercise, you will not need to change any code in this file, or any
#  other files other than those mentioned above.
#

import numpy as np
import scipy.optimize as spo
import matplotlib.pyplot as plt
from plot_data import plot_data
from cost_function import cost_function
from plot_decision_boundary import plot_decision_boundary
from sigmoid import sigmoid
from predict import predict

# Load Data
# The first two columns contains the exam scores and the third column contains
# the label.
data = np.loadtxt('../ex2/ex2data1.txt', delimiter=',')
m = data.shape[0]
X = np.array(data[:, 0:2]).reshape([m, 2])
y = np.array(data[:, 2]).reshape([m, 1])

# ====================== Part 1: Plotting =====================================
#  We start the exercise by first plotting the data to understand the problem
#  we are working with.

# Plot data with legends and labels
plot_data(X, y)
plt.hold(True)
plt.xlabel('Exam 1 score')
plt.ylabel('Exam 2 score')
plt.legend(['Admitted (y=1)', 'Not admitted (y=0)'])
plt.hold(False)

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ====================== Part 2: Compute Cost and Gradient ====================
#  In this part of the exercise, you will implement the cost and gradient
#  for logistic regression. You neeed to complete the code in cost_function.py

# Add intercept term to X
X = np.hstack([np.ones([m, 1]), X])
n = X.shape[1]

# Initialize fitting parameters
init_theta = np.zeros([n, 1])

# Compute and display initial cost and gradient
[cost, grad] = cost_function(init_theta, X, y)

print('Cost at initial theta (zeros): {}'.format(cost))
print('Gradient at initial theta (zeros): \n{}\n'.format(grad.T))

input('Program paused. Press enter to continue.')

# ====================== Part 3: Optimizing using fminunc  ====================
#  In this exercise, you will use a built-in function (fminunc) to find the
#  optimal parameters theta.

# Run truncated Newton method from scipy.optimize to obtain the optimal theta.
theta, _, _ = spo.fmin_tnc(func=lambda t, X, y: cost_function(t, X, y)[0],
                     fprime=lambda t, X, y: cost_function(t, X, y)[1].ravel(),
                     x0=init_theta.ravel(), args=(X, y), disp=False)
[cost, grad] = cost_function(theta, X, y)

# Print theta to screen
print('Cost at theta found by fmin_tnc(...): {}'.format(cost))
print('Theta at optimum: \n{}\n'.format(theta.T))

# Plot boundary with legends and labels
plot_decision_boundary(theta, X, y)
plt.hold(True)
plt.xlabel('Exam 1 score')
plt.ylabel('Exam 2 score')
plt.legend(['Admitted (y=1)', 'Not admitted (y=0)', 'Decision Boundary'])
plt.hold(False)

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ====================== Part 4: Predict and Accuracies =======================
#  After learning the parameters, you'll like to use it to predict the outcomes
#  on unseen data. In this part, you will use the logistic regression model
#  to predict the probability that a student with score 45 on exam 1 and  score
#  85 on exam 2 will be admitted.
#
#  Furthermore, you will compute the training and test set accuracies of our
#  model.
#
#  Your task is to complete the code in predict.m

#  Predict probability for a student with score 45 on exam 1 and score 85 on 2
prob = float(sigmoid(np.array([1, 45, 85], ndmin=2) @ theta))
print('For a student with scores 45 and 85, we predict an admission '
      'probability of {0:.4f}'.format(prob))

# Compute accuracy on our training set
correct = np.where(predict(theta, X) == y)
accuracy = float(correct[0].size / y.size)
print('Training accuracy: {0:.2%}\n'.format(accuracy))
