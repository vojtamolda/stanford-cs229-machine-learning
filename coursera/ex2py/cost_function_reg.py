import numpy as np
from sigmoid import sigmoid


def cost_function_reg(theta, X, y, rlambda):
    """Computes the cost of using theta as the parameter for logistic
     regression and the gradient of the cost w.r.t. to the parameters with
     regularization .
     """

    # Initialize some useful values
    m, n = X.shape
    theta = theta.reshape([n, 1])

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Compute the cost of a particular choice of theta. You
    #               should set J to the cost. Compute the partial derivatives
    #               and set grad to the partial derivatives of the cost
    #               w.r.t. each parameter in theta
    #
    # Note: grad should have the same dimensions as theta
    #

    from cost_function import cost_function
    J, grad = cost_function(theta, X, y)

    rtheta = np.vstack([[[0]], theta[1:, :]])
    J += (rtheta.T @ rtheta) * rlambda / 2 / m
    grad += rtheta * rlambda / m

    # =========================================================================

    # You need to return the following variables correctly
    return float(J), grad
