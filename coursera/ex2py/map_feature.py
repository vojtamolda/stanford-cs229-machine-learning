import numpy as np


def map_feature(t1, t2):
    """Maps the two linear input features to quadratic features used in the
     regularization exercise.
     t1, t2: Must be the same shape
     out: A new feature array with more features, comprising of
          t1, t2, t1^2, t2^2, t1*t2, t1*t2^2, ...
     """

    t1 = np.atleast_2d(t1).T
    t2 = np.atleast_2d(t2).T
    out = np.ones([t1.shape[0], 1])
    degree = 6

    for t12pow in range(1, degree + 1):
        for t2pow in range(0, t12pow + 1):
            tpwr = np.power(t1, t12pow - t2pow) * np.power(t2, t2pow)
            out = np.hstack([out, tpwr])

    return out
