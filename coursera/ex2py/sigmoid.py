import numpy as np


def sigmoid(z):
    """Compute sigmoid/logistic function of z."""

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Compute the sigmoid of each value of z (z can be a matrix,
    #               vector or scalar).

    # g = np.zeros(z.shape)
    g = 1 / (1 + np.exp(-z))

    # =========================================================================

    # You need to return the following variables correctly
    return g
