import numpy as np
import matplotlib.pyplot as plt


def plot_data(X, y):
    """Plots the data points X and y into a new figure. Data points with + for
     the positive examples and o for the negative examples. X is assumed to be
     a Mx2 matrix.
     """

    plt.figure()
    plt.hold(True)

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Plot the positive and negative examples on a 2D plot, using
    #               the option 'k+' for the positive, examples and 'ko' for
    #               the negative examples.
    #

    admitted = np.where(y == 1)
    declined = np.where(y == 0)

    plt.plot(X[admitted[0], 0], X[admitted[0], 1], 'k+',
             linewidth=2, markerfacecolor='black', markersize=7)
    plt.plot(X[declined[0], 0], X[declined[0], 1], 'ko',
             linewidth=2, markerfacecolor='yellow', markersize=7)

    # =========================================================================

    plt.hold(False)
