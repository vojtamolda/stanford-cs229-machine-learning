# Machine Learning Online Class
#  Exercise 2: Logistic Regression
#
#  Instructions
#  ------------
#
#  This file contains code that helps you get started on the logistic regression
#  exercise. You will need to complete the following functions in this exericse:
#
#     cost_function_reg.py
#
#  For this exercise, you will not need to change any code in this file, or any
#  other files other than those mentioned above.
#

import numpy as np
import scipy.optimize as spo
import matplotlib.pyplot as plt
from plot_data import plot_data
from map_feature import map_feature
from cost_function_reg import cost_function_reg
from plot_decision_boundary import plot_decision_boundary
from predict import predict

# Load Data
# The first two columns contains the X values and the third column contains
# the label.
data = np.loadtxt('../ex2/ex2data2.txt', delimiter=',')
m = data.shape[0]
X = np.array(data[:, 0:2]).reshape([m, 2])
y = np.array(data[:, 2]).reshape([m, 1])

# Plot data with legends and labels
plot_data(X, y)
plt.hold(True)
plt.xlabel('Microchip Test 1')
plt.ylabel('Microchip Test 2')
plt.legend(['y = 1', 'y = 0'])
plt.hold(False)

# ====================== Part 1: Regularized Logistic Regression ==============
#  In this part, you are given a dataset with data points that are not linearly
#  separable. However, you would still like to use logistic regression to 
#  classify the data points.
#
#  To do so, you introduce more features to use -- in particular, you add
#  polynomial features to our data matrix (similar to polynomial regression).
#

# Add Polynomial Features
# Note that mapFeature also adds a column of ones for us, so the intercept
# term is handled
X = map_feature(X[:, 0], X[:, 1])
n = X.shape[1]

# Initialize fitting parameters
init_theta = np.zeros([n, 1])

# Set regularization parameter lambda to 1
rlambda = 1

# Display initial cost and gradient for regularized logistic regression
[cost, grad] = cost_function_reg(init_theta, X, y, rlambda)
print('Cost at initial theta (zeros): {}\n'.format(cost))

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ====================== Part 2: Regularization and Accuracies ================
#  Optional Exercise:
#  In this part, you will get to try different values of lambda and see how
#  regularization affects the decision coundart
#
#  Try the following values of lambda (0, 1, 10, 100).
#
#  How does the decision boundary change when you vary lambda? How does the
#  training set accuracy vary?
#

# Initialize fitting parameters
init_theta = np.zeros([n, 1])

# Set regularization parameter lambda to 1 (you should vary this)
rlambda = 1

# Run Newton-CG method from scipy.optimize to obtain the optimal theta.
theta = spo.fmin_ncg(f=lambda t, X, y, l: cost_function_reg(t, X, y, l)[0],
                     fprime=lambda t, X, y, l:
                              cost_function_reg(t, X, y, l)[1].ravel(),
                     x0=init_theta.ravel(), args=(X, y, rlambda), disp=False)

# Plot boundary with legends and labels
plot_decision_boundary(theta, X, y)
plt.hold(True)
plt.title('lambda = {}'.format(rlambda))
plt.xlabel('Microchip Test 1')
plt.ylabel('Microchip Test 2')
plt.legend(['y = 1', 'y = 0', 'Decision boundary'])
plt.hold(False)

# Compute accuracy on our training set
correct = np.where(predict(theta, X) == y)
accuracy = float(correct[0].size / y.size)
print('Train accuracy: {0:.2%}\n'.format(accuracy))

print('Program paused. Close the plot window to continue.\n')
plt.show()
