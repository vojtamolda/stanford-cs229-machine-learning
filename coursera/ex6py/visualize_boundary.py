import numpy as np
import matplotlib.pyplot as plt
from svm_predict import svm_predict
from plot_data import plot_data


def visualize_boundary(X, y, model):
    """Plots a non-linear decision boundary learned by the SVM and overlays
     the data on it
     """

    # Plot the training data on top of the boundary
    plot_data(X, y)

    # Make classification predictions over a grid of values
    x1plot = np.linspace(X[:, 0].min(), X[:, 1].max(), 300)
    x2plot = np.linspace(X[:, 1].min(), X[:, 0].max(), 300)
    x1, x2 = np.meshgrid(x1plot, x2plot)
    preds = svm_predict(model, np.c_[x1.ravel(), x2.ravel()])
    preds = preds.reshape(x1.shape)

    # Plot a single contour at the SVM decision boundary
    plt.hold(True)
    plt.contour(x1, x2, preds, [0], color='b')
    plt.hold(False)
