import re
import numpy as np
from stemming.porter import stem
from get_vocab_dict import get_vocab_dict


def process_email(email_contents):
    """Preprocesses the body of an email and returns a list of indices of the
     words contained in the email.
     """

    # Load vocabulary
    vocab_dict = get_vocab_dict()

    # Init return value
    word_indices = []

    # ======================= Preprocess Email ================================

    # Find the Headers ( \n\n and remove )
    # Uncomment the following lines if you are working with raw emails with the
    # full headers

    # hdrstart = strfind(email_contents, ([char(10) char(10)]))
    # email_contents = email_contents(hdrstart(1):end)

    # Lower case
    email_contents = email_contents.lower()

    # Strip all HTML
    # Looks for any expression that starts with < and ends with > and replace
    # and does not have any < or > in the tag it with a space
    email_contents = re.sub(r'<[^<>]+>', ' ', email_contents)

    # Handle Numbers
    # Look for one or more characters between 0-9
    email_contents = re.sub(r'[\d\']+', ' number ', email_contents)

    # Handle URLS
    # Look for strings starting with http:// or https://
    email_contents = re.sub(r'(http|https)://[^\s]*', ' httpaddr ', email_contents)

    # Handle Email Addresses
    # Look for strings with @ in the middle
    email_contents = re.sub('[^\s]+@[^\s]+', ' emailaddr ', email_contents)

    # Handle $ sign
    email_contents = re.sub('[$]+', ' dollar ', email_contents)

    # ======================= Tokenize Email ==================================

    # Output the email to screen as well
    print('\n===================== Processed Email ==========================')

    # Split email into words
    words = re.findall(r'[\w\']+', email_contents)

    chars = 0
    # Process all words
    for word in words:

        # Trim whitespaces around the word
        word = word.strip()

        # Remove any non alphanumeric characters
        word = re.sub('[^a-zA-Z0-9]', '', word)

        # Porter stem the word
        word = stem(word)

        # Skip the word if it is too short
        if len(word) == 0: continue

        # Look up the word in the dictionary and add to word_indices if
        # found
        # =================== YOUR CODE HERE ==================================
        # Instructions: Fill in this to add the index of word toword_indices
        #               if it is in the vocabulary. At this point of the code,
        #               you have a stemmed word from the email in the variable
        #               word. You should look up word in the vocabulary dict
        #               (vocab_dict). If a match exists, you should add the
        #               index of the word to the word_indices vector.
        #               Concretely, if word = 'action', then you should look
        #               up the vocabulary dict to find the index of 'action'.
        #               For example, if vocab_dict['action'] = 18., then you
        #               should add 18 to the word_indices vector (e.g.,
        #               word_indices.append(18)).
        #
        # Note: vocab_list[word] returns a the index in the vocabulary list.
        #

        if word in vocab_dict:
            word_idx = vocab_dict[word]
            word_indices.append(word_idx)

        # =====================================================================

        # Print to screen, ensuring that the output lines are not too long
        if (chars + len(word) + 1) > 78:
            print()
            chars = 0
        print(word + ' ', end='')
        chars += len(word) + 1

    # Print footer
    print('\n================================================================')

    return word_indices
