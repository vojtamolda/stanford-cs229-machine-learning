import numpy as np


def gaussian_kernel(X1, X2, sigma):
    """Returns a gaussian kernel between X2 and X2. X1 is a m1 x n matrix and
     X2 is a m2 x n matrix. Returned value is m1 x m2 matrix."""

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Fill in this function to return the similarity between X1
    #               and X2 computed using a Gaussian kernel with bandwidth
    #               sigma.
    #
    # Note: This code is super hackish in Python and non-vectorized version
    #       tends to run for 15 minutes instead of a minute. Vectorization
    #       is quite tricky for X1 and X2 of different shapes. The solution
    #       used here comes from: http://stackoverflow.com/questions/29731726/
    #

    diffs = np.linalg.norm(X2[None, :, :] - X1[:, None, :], axis=2)
    kernel = np.exp( - (diffs * diffs) / 2 / sigma**2)

    # =========================================================================

    return kernel
