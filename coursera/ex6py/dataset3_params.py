import numpy as np
from gaussian_kernel import gaussian_kernel
from svm_predict import svm_predict
from svm_train import svm_train


def dataset3_params(X, y, Xval, yval):
    """Returns your choice of C and sigma. You should complete this function
     to return the optimal C and sigma based on a cross-validation set.
     """

    # You need to return the following variables correctly.
    C, sigma = 1, 0.3

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Fill in this function to return the optimal C and sigma
    #               learning parameters found using the cross validation set.
    #               You can use svm_predict to predict the labels on the cross
    #               validation set. For example this code will return the
    #               predictions on the cross validation set:
    #
    #                  # ypred = svm_predict(model, Xval)
    #
    #  Note: You can compute the prediction error using
    #        (ypred != yval).mean()
    #

    m, n = X.shape
    opts = np.array([0.01, 0.03, 0.06, 0.1, 0.3, 0.6, 1, 3, 6, 10, 30])

    accuracy = np.zeros([opts.size, opts.size])
    precision = np.zeros([opts.size, opts.size])
    recall = np.zeros([opts.size, opts.size])
    fscore = np.zeros([opts.size, opts.size])

    for i, C in enumerate(opts):
        for j, sigma in enumerate(opts):

            model = svm_train(X, y, C,
                              lambda x1, x2: gaussian_kernel(x1, x2, sigma))
            ypred = svm_predict(model, Xval)

            true_positives = np.logical_and((yval == 1), (ypred == 1)).sum()
            true_negatives = np.logical_and((yval == 0), (ypred == 0)).sum()
            false_positives = np.logical_and((yval == 1), (ypred == 0)).sum()
            false_negatives = np.logical_and((yval == 0), (ypred == 1)).sum()

            accuracy[i, j] = (true_positives + true_negatives) / m
            precision[i, j] = (true_positives /
                              (true_positives + false_positives))
            recall[i, j] = (true_positives /
                           (true_positives + false_negatives))
            fscore[i, j] = (2 * precision[i, j] * recall[i, j] /
                           (precision[i, j] + recall[i, j]))

    # import matplotlib.pyplot as plt
    # import mpl_toolkits.mplot3d
    # CC, ssigma = np.meshgrid(opts, opts)
    #
    # ax = plt.figure().gca(projection='3d')
    # ax.plot_surface(CC, ssigma, accuracy, cmap='hot', cstride=1, rstride=1)
    # plt.title('Accuracy'); plt.xlabel('C'); plt.ylabel('sigma')
    #
    # ax = plt.figure().gca(projection='3d')
    # ax.plot_surface(CC, ssigma, precision, cmap='hot', cstride=1, rstride=1)
    # plt.title('Precision'); plt.xlabel('C'); plt.ylabel('sigma')
    #
    # ax = plt.figure().gca(projection='3d')
    # ax.plot_surface(CC, ssigma, recall, cmap='hot', cstride=1, rstride=1)
    # plt.title('Recall'); plt.xlabel('C'); plt.ylabel('sigma')
    #
    # ax = plt.figure().gca(projection='3d')
    # ax.plot_surface(CC, ssigma, fscore, cmap='hot', cstride=1, rstride=1)
    # plt.title('F-Score'); plt.xlabel('C'); plt.ylabel('sigma')
    #
    # plt.show()

    imax, jmax = np.unravel_index(np.argmax(accuracy), accuracy.shape)
    C, sigma = opts[imax], opts[jmax]

    # =========================================================================

    return C, sigma
