import numpy as np


def linear_kernel(X1, X2):
    """Returns a linear kernel between X2 and X2. X1 is a m1 x n matrix and X2
     is a m2 x n matrix. Returned value is m1 x m2 matrix."""

    # Compute the kernel
    kernel = X1 @ X2.T

    return kernel
