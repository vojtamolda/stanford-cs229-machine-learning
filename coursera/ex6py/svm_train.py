import numpy as np
import sklearn.svm as svm


def svm_train(X, y, C, kernel='linear', tol=1e-3, max_iter=-1):
    """Trains an SVM classifier and returns trained model. X is the matrix of
     training examples. Each row is a training example, and the j-th column
     holds the j-th feature. y is a column matrix containing 1 for positive
     examples and 0 for negative examples. C is the standard SVM regularization
     parameter. tol is a tolerance value used for determining equality of
     floating point numbers. max_iter controls the number of iterations
     over the dataset (without changes to alpha) before the algorithm quits.
    """

    model = svm.SVC(C=C, kernel=kernel, tol=tol, max_iter=max_iter)
    model.fit(X, y.ravel())

    return model
