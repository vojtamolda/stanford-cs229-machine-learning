import numpy as np
import sklearn.svm as svm


def svm_predict(model, X):
    """Returns a vector of predictions using a trained SVM model. X is a m x n
     matrix where there each example is a row. model is a svm model returned
     from svm_train. Return value is a m x 1 column of predictions of {0, 1}
     values.
     """

    pred = model.predict(X)
    pred = np.atleast_2d(np.sign(pred)).T
    return pred
