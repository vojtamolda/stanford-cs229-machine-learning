# Machine Learning Online Class
#  Exercise 6 - Spam Classification with SVMs
#
#  Instructions
#  ------------
#
#  This file contains code that helps you get started on the exercise. You
#  will need to complete the following functions:
#
#     email_file.py
#
#  For this exercise, you will not need to change any code in this file, or
#  any other files other than those mentioned above.
#

import numpy as np
import scipy.io as scio
from svm_train import svm_train
from svm_predict import svm_predict
from read_file import read_file
from process_email import process_email
from email_features import email_features
from get_vocab_dict import get_vocab_dict

# ================= Part 1: Email Preprocessing ===============================
#  To use an SVM to classify emails into Spam v.s. Non-Spam, you first need to
#  convert each email into a vector of features. In this part, you will
#  implement the preprocessing steps for each email. You should complete the
#  code in process_email.py to produce a word indices vector for a given email.
#

print('Preprocessing sample email (emailSample1.txt)...')

# Extract Features
file_contents = read_file('../ex6/emailSample1.txt')
word_indices = process_email(file_contents)

# Print Stats
print('Word Indices: {}\n'.format(word_indices))

input('Program paused. Press enter to continue.\n')

# ================= Part 2: Feature Extraction ================================
#  Now, you will convert each email into a vector of features in R^n. You
#  should complete the code in email_features.py to produce a feature vector
#  for a given email.
#

print('Extracting features from sample email (emailSample1.txt)...')

# Extract features
file_contents = read_file('../ex6/emailSample1.txt')
word_indices = process_email(file_contents)
features = email_features(word_indices)

# Print stats
print('\nLength of feature vector: {}'.format(len(features)))
print('Number of non-zero entries: {}\n'.format((features > 0).sum()))

input('Program paused. Press enter to continue.\n')

# ================= Part 3: Train Linear SVM for Spam Classification ==========
#  In this section, you will train a linear classifier to determine if an email
#  is spam or not-spam.
#

print('Training Linear SVM for Spam Classification (this may take a minute)...')

# Load the training spam email dataset
data = scio.loadmat('../ex6/spamTrain.mat')
X, y = data['X'], data['y']

# Train and predict on training dataset
C = 0.1
model = svm_train(X, y, C)
pred = svm_predict(model, X)

correct = np.where((pred == y))
accuracy = float(correct[0].size / y.size)
print('Training Accuracy: {:0.2%}\n'.format(accuracy))

# =================== Part 4: Test Spam Classification ========================
#  After training the classifier, we can evaluate it on a test set. We have
#  included a test set in spamTest.mat.
#

print('Evaluating the trained Linear SVM on a test set...')

# Load the test spam email dataset
data = scio.loadmat('../ex6/spamTest.mat')
Xtest, ytest = data['Xtest'], data['ytest']

# Predict on test dataset
pred = svm_predict(model, Xtest)

correct = np.where(pred == ytest)
accuracy = float(correct[0].size / ytest.size)
print('Test Accuracy: {:0.2%}\n'.format(accuracy))

# ================= Part 5: Top Predictors of Spam ============================
#  Since the model we are training is a linear SVM, we can inspect the weights
#  learned by the model to understand better how it is determining whether an
#  email is spam or not. The following code finds the words with the highest
#  weights in the classifier. Informally, the classifier 'thinks' that these
#  words are the most likely indicators of spam.
#

# Obtain the vocabulary dictionary and invert it
vocab_dict = get_vocab_dict()
index_dict = {idx: word for word, idx in vocab_dict.items()}

# Descending sort of the weights
sorted_idx = np.argsort(model.coef_)[:, ::-1]

print('Top predictors of spam:')
for i in range(20):
    word = index_dict[sorted_idx[0, i]]
    coef = model.coef_[:, sorted_idx[0, i]]
    print(' ({:0.5f})\t{}'.format(float(coef), word))

input('\nProgram paused. Press enter to continue.\n')

# ================= Part 6: Try Your Own Emails ===============================
#  Now that you've trained the spam classifier, you can use it on your own
#  emails! In the starter code, we have included spamSample1.txt,
#  spamSample2.txt, emailSample1.txt and emailSample2.txt as examples. The
#  following code reads in one of these emails and then uses your learned SVM
#  classifier to determine whether the email is spam or not spam.
#

# Set the file to be read in (change this to spamSample2.txt, emailSample1.txt
# or emailSample2.txt to see different predictions on different emails
# types). Try your own emails as well!
filename = '../ex6/spamSample1.txt'
print('Processing email ({})...'.format(filename))

# Extract features
file_contents = read_file(filename)
word_indices = process_email(file_contents)
features = email_features(word_indices)

# Predict
pred = svm_predict(model, features)

print('Spam Classification: {}\n'
      '(1 indicates spam, 0 indicates not spam)'.format(int(pred)))
