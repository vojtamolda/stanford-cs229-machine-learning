

def read_file(filename):
    """Reads a file and returns its entire contents."""

    # File contents
    file_contents = ""

    try:
        # Read the entire file
        with open(filename) as file:
            file_contents = file.read()
    except Exception as exc:
        # Ooops...
        print(exc)
        exit(3)
    finally:
        return file_contents
