

def get_vocab_dict():
    """Reads the fixed vocabulary list in vocab.txt and returns a dictionary
     of the words that appear there.
    """

    # Create a dictionary
    vocab_dict = {}

    # Open the fixed vocabulary file
    with open('../ex6/vocab.txt') as file:

        # Read lines into the dictionary
        for line in file:
            words = line.split()
            vocab_dict[words[1]] = int(words[0])
        file.close()

    return vocab_dict
