# Machine Learning Online Class
#  Exercise 6 - Support Vector Machines
#
#  Instructions
#  ------------
# 
#  This file contains code that helps you get started on the exercise. You
#  will need to complete the following functions:
#
#     kernel.py
#     dataset3_params.py
#
#  For this exercise, you will not need to change any code in this file, or
#  any other files other than those mentioned above.
#

import numpy as np
import scipy.io as scio
import matplotlib.pyplot as plt
from plot_data import plot_data
from svm_train import svm_train
from svm_predict import svm_predict
from linear_kernel import linear_kernel
from gaussian_kernel import gaussian_kernel
from visualize_boundary import visualize_boundary
from dataset3_params import dataset3_params

# ================= Part 1: Loading and Visualizing Data ======================
#  We start the exercise by first loading and visualizing the dataset. The
#  following code will load the dataset into your environment and plot the
#  data.
#

# Load Training Data
print('Loading and Visualizing Data...\n')

# Load X, y
data = scio.loadmat('../ex6/ex6data1.mat')
X, y = data['X'], data['y']

# Plot training data
plot_data(X, y)

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ================= Part 2: Training Linear SVM ===============================
#  The following code will train a linear SVM on the dataset and plot the
#  decision boundary learned.
#

print('Training Linear SVM...\n')

# You should try to change the C value below and see how the decision
# boundary varies (e.g., try C = 1000)
C = 1
model = svm_train(X, y, C, linear_kernel)
visualize_boundary(X, y, model)

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ================= Part 3: Implementing Gaussian Kernel ======================
#  You will now implement the Gaussian kernel to use with the SVM. You should
#  complete the code in kernel.py
#

print('Evaluating the Gaussian Kernel...')

x1 = np.array([[1, 2, 1]])
x2 = np.array([[0, 4, -1]])
sigma = 2
sim = float(gaussian_kernel(x1, x2, sigma))

print('Gaussian Kernel between x1=[1, 2, 1], x2=[0, 4, -1], sigma=2: {:.6f}\n'
      '(this value should be about 0.324652)\n'.format(sim))

input('Program paused. Press enter to continue.\n')

# ================= Part 4: Visualizing Dataset 2 =============================
#  The following code will load the next dataset into your environment and 
#  plot the data.
#

print('Loading and Visualizing Data ...\n')

# Load X, y
data = scio.loadmat('../ex6/ex6data2.mat')
X, y = data['X'], data['y']

# Plot training data
plot_data(X, y)

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ================= Part 5: Training SVM with RBF Kernel (Dataset 2) ==========
#  After you have implemented the kernel, we can now use it to train the 
#  SVM classifier.
#

print('Training SVM with RBF kernel (this may take a minute)...\n')

# SVM Parameters
C, sigma = 1, 0.1

# Train and visualize the SVM
model = svm_train(X, y, C, lambda x1, x2: gaussian_kernel(x1, x2, sigma))
visualize_boundary(X, y, model)

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ================= Part 6: Visualizing Dataset 3 =============================
#  The following code will load the next dataset into your environment and 
#  plot the data. 
#

print('Loading and Visualizing Data...\n')

# Load X, y, Xval, yval
data = scio.loadmat('../ex6/ex6data3.mat')
X, y = data['X'], data['y']
Xval, yval = data['Xval'], data['yval']

# Plot training data
plot_data(X, y)

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ================= Part 7: Training SVM with RBF Kernel (Dataset 3) ==========
#  This is a different dataset that you can use to experiment with. Try
#  different values of C and sigma here.
#

# Try different SVM parameters here
C, sigma = dataset3_params(X, y, Xval, yval)

# Train and visualize the SVM
model = svm_train(X, y, C, lambda x1, x2: gaussian_kernel(x1, x2, sigma))
visualize_boundary(X, y, model)

print('Program paused. Close the plot window to continue.\n')
plt.show()
