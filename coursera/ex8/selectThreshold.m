function [bestEpsilon bestF1] = selectThreshold(yval, pval)
%SELECTTHRESHOLD Find the best threshold (epsilon) to use for selecting
%outliers
%   [bestEpsilon bestF1] = SELECTTHRESHOLD(yval, pval) finds the best
%   threshold to use for selecting outliers based on the results from a
%   validation set (pval) and the ground truth (yval).
%

steps = 1000;
stepsize = (max(pval) - min(pval)) / steps;
epsilon = [min(pval):stepsize:max(pval)];

accuracy_plot = ones(size(epsilon));
precision_plot = ones(size(epsilon));
recall_plot = ones(size(epsilon));
fscore_plot = ones(size(epsilon));

for i = 1:steps+1
    % ====================== YOUR CODE HERE ======================
    % Instructions: Compute the F1 score of choosing epsilon as the
    %               threshold and place the value in F1. The code at the
    %               end of the loop will compare the F1 score for this
    %               choice of epsilon and set it to be the best epsilon if
    %               it is better than the current choice of epsilon.
    %               
    % Note: You can use predictions = (pval < epsilon) to get a binary vector
    %       of 0's and 1's of the outlier predictions
    
    normal = (pval >= epsilon(i));
    anomalous = (pval < epsilon(i));

    truePositives = sum((yval == 1) & anomalous);
    trueNegatives = sum((yval == 0) & normal);
    falsePositives = sum((yval == 0) & anomalous);
    falseNegatives = sum((yval == 1) & normal);

    accuracy_plot(i) = (truePositives + trueNegatives) / numel(yval);
    precision_plot(i) = truePositives / (truePositives + falsePositives);
    recall_plot(i) = truePositives / (truePositives + falseNegatives);
    fscore_plot(i) = 2 * precision_plot(i) * recall_plot(i) ...
                   / (precision_plot(i) + recall_plot(i));
    
    % =============================================================

end

% figure; hold on;
% plot(epsilon, accuracy_plot, 'r');
% plot(epsilon, precision_plot, 'm');
% plot(epsilon,  recall_plot, 'c');
% plot(epsilon, fscore_plot, 'b');
% title('Performance Metrics')
% xlabel('epsilon')
% legend('Accuracy', 'Precision', 'Recall', 'F-Score');
% hold off;

[~, idx] = max(fscore_plot);
bestF1 = fscore_plot(idx);
bestEpsilon = epsilon(idx);

end
