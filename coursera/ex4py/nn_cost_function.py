import numpy as np
from sigmoid import sigmoid
from sigmoid_gradient import sigmoid_gradient

def nn_cost_function(nn_params, input_layer_size, hidden_layer_size,
                     num_labels, X, y, rlambda):
    """Implements the neural network cost function for a two layer neural
     network which performs classification. Computes the cost and gradient of
     the neural network. The parameters for the neural network are "unrolled"
     into the vector nn_params and need to be converted back into the weight
     matrices.

     The returned parameter grad should be a "unrolled" vector of the partial
     derivatives of the neural network.
     """

    # Reshape nn_params back into the parameters Theta1 and Theta2, the weight
    # matrices for our 2 layer neural network.
    split = hidden_layer_size * (input_layer_size + 1)
    Theta1 = nn_params[:split].reshape([hidden_layer_size, input_layer_size + 1])
    Theta2 = nn_params[split:].reshape([num_labels, hidden_layer_size + 1])

    # Setup some useful variables
    m, n = X.shape

    # You need to return the following variables correctly
    grad_Theta1 = np.zeros(Theta1.shape)
    grad_Theta2 = np.zeros(Theta2.shape)
    J = 0

    # ======================= YOUR CODE HERE ==================================
    # Instructions: You should complete the code by working through the
    #               following parts.
    #
    # Part 1: Feed-forward the neural network and return the cost in the
    #         variable J. After implementing Part 1, you can verify that your
    #         cost function computation is correct by verifying the cost
    #         computed in ex4.py
    #
    # Part 2: Implement the back-propagation algorithm to compute the gradients
    #         grad_Theta1 and grad_Theta2. You should return the partial
    #         derivatives of the cost function with respect to Theta1 and
    #         Theta2 in grad_Theta1 and grad_Theta2, respectively. After
    #         implementing Part 2, you can check that your implementation is
    #         correct by running check_nn_gradients.
    #
    #         Note: The vector y passed into the function is a vector of labels
    #               containing values from 1..K. You need to map this vector
    #               into a binary vector of 1's and 0's to be used with the
    #               neural networkcost function.
    #
    #         Hint: We recommend implementing back-propagation using a for-loop
    #               over the training examples if you are implementing it for
    #               the first time.
    #
    # Part 3: Implement regularization with the cost function and gradients.
    #         Hint: You can implement this around the code for
    #               back-propagation. That is, you can compute the gradients
    #               for the regularization separately and then add them to
    #               grad_Theta1 and grad_Theta2 from Part 2.
    #

    a1 = np.hstack([np.ones([m, 1]), X])

    z2 = a1 @ Theta1.T
    a2 = np.hstack([np.ones([m, 1]), sigmoid(z2)])

    z3 = a2 @ Theta2.T
    a3 = sigmoid(z3)

    h = a3

    Y = np.zeros(h.shape)
    for i in range(m):
        Y[i, y[i, 0] - 1] = 1

    rTheta1 = np.hstack([np.zeros([hidden_layer_size, 1]), Theta1[:, 1:]])
    rTheta2 = np.hstack([np.zeros([num_labels, 1]), Theta2[:, 1:]])

    J = ( - (Y * np.log(h) + (1 - Y) * np.log(1 - h)).sum()
          + ((rTheta1**2).sum() + (rTheta2**2).sum()) * rlambda / 2 ) / m

    # ------------------------------------------------------------------------

    delta3 = a3 - Y
    grad_Theta2 = (delta3.T @ a2 + rTheta2 * rlambda) / m

    delta2 = delta3 @ Theta2
    delta2 = delta2[:, 1:] * sigmoid_gradient(z2)
    grad_Theta1 = (delta2.T @ a1 + rTheta1 * rlambda) / m

    # =========================================================================

    # Unroll gradients
    grad = np.concatenate([grad_Theta1.ravel(), grad_Theta2.ravel()])

    return float(J), grad
