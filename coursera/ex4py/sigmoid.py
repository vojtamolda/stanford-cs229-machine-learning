import numpy as np


def sigmoid(z):
    """Compute sigmoid/logistic function of z."""

    g = 1 / (1 + np.exp(-z))

    return g
