# Machine Learning Online Class
#  Exercise 4: Neural Network Learning
#
#  Instructions
#  ------------
# 
#  This file contains code that helps you get started on the linear exercise.
#  You will need to complete the following functions in this exercise:
#
#     sigmoid_gradient.py
#     rand_initialize_weights.py
#     nn_cost_function.py
#
#  For this exercise, you will not need to change any code in this file, or
#  any other files other than those mentioned above.
#

import random
import numpy as np
import scipy.io as scio
import scipy.optimize as spo
import matplotlib.pyplot as plt
from display_data import display_data
from nn_cost_function import nn_cost_function
from sigmoid_gradient import sigmoid_gradient
from rand_initialize_weights import rand_initialize_weights
from check_nn_gradients import check_nn_gradients
from predict import predict
from sigmoid import sigmoid

# Setup the parameters you will use for this part of the exercise
input_layer_size = 400  # 20x20 Input Images of Digits
hidden_layer_size = 25  # 25 hidden units
num_labels = 10  # Labels 1 to 10 (note that we have mapped "0" to label 10)

# ====================== Part 1: Loading and Visualizing Data =================
#  We start the exercise by first loading and visualizing the dataset.
#  You will be working with a dataset that contains handwritten digits.
#

# Load Training Data
print('Loading and Visualizing Data...\n')

data = scio.loadmat('../ex4/ex4data1.mat')
X, y = data['X'], data['y']
m, n = X.shape

# Randomly select 100 data points to display
rand_indices = [random.randint(0, m - 1) for _ in range(100)]
rand_selection = X[rand_indices[:100], :]
display_data(rand_selection)

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ====================== Part 2: Loading Parameters ===========================
#  In this part of the exercise, we load some pre-initialized neural network
#  parameters.
#

# Load the weights into variables Theta1 and Theta2
print('Loading Saved Neural Network Parameters...')

data = scio.loadmat('../ex4/ex4weights.mat')
Theta1, Theta2 = data['Theta1'], data['Theta2']

# Unroll parameters 
nn_params = np.concatenate([Theta1.ravel(), Theta2.ravel()])

# ====================== Part 3: Compute Cost (Feed-forward) ==================
#  To the neural network, you should first start by implementing the feed-
#  forward part of the neural network that returns the cost only. You should
#  complete the code in nnCostFunction.m to return cost. After implementing
#  the feedforward to compute the cost, you can verify that your implementation
#  is correct by verifying that you get the same cost as us for the fixed 
#  debugging parameters.
#
#  We suggest implementing the feed-forward cost *without* regularization
#  first so that it will be easier for you to debug. Later, in part 4, you
#  will get to implement the regularized cost.
#

print('Checking Cost Function...')

# Weight regularization parameter (we set this to 0 here).
rlambda = 0

J, _ = nn_cost_function(nn_params, input_layer_size, hidden_layer_size,
                     num_labels, X, y, rlambda)

print('Cost at parameters (loaded from ex4weights): {0:.6f}\n'
      '(This value should be about 0.287629)\n'.format(J))

input('Program paused. Press enter to continue.\n')

# ====================== Part 4: Implement Regularization =====================
#  Once your cost function implementation is correct, you should now continue
#  to implement the regularization with the cost.
#

print('Checking Cost Function (w/ Regularization)...')

# Weight regularization parameter (we set this to 1 here).
rlambda = 1

J, _ = nn_cost_function(nn_params, input_layer_size, hidden_layer_size,
                     num_labels, X, y, rlambda)

print('Cost at parameters (loaded from ex4weights): {0:.6f}\n'
      '(This value should be about 0.383770)\n'.format(J))

input('Program paused. Press enter to continue.\n')

# ====================== Part 5: Sigmoid Gradient  ============================
#  Before you start implementing the neural network, you will first implement
#  the gradient for the sigmoid function. You should complete the code in
#  the sigmoid_gradient.py file.
#

print('Evaluating Sigmoid Gradient...')

g = sigmoid_gradient(np.array([-1, -0.5, 0, 0.5, 1]))
print('Sigmoid gradient evaluated at [-1, -0.5, 0, +0.5 +1]: \n{}\n'.format(g))

input('Program paused. Press enter to continue.\n')

# ====================== Part 6: Initializing Parameters ======================
#  In this part of the exercise, you will be starting to implment a two layer
#  neural network that classifies digits. You will start by implementing a
#  function to initialize the weights of the neural network in file
#  rand_initialize_weights.py
#

print('Initializing Neural Network Parameters...')

init_Theta1 = rand_initialize_weights(input_layer_size, hidden_layer_size)
init_Theta2 = rand_initialize_weights(hidden_layer_size, num_labels)

# Unroll parameters
init_nn_params = np.concatenate([init_Theta1.ravel(), init_Theta2.ravel()])

# ====================== Part 7: Implement Backpropagation ====================
#  Once your cost matches up with ours, you should proceed to implement the
#  back-propagation algorithm for the neural network. You should add to the
#  code you've written in nnCostFunction.m to return the partial derivatives
#  of the parameters.
#

print('Checking Back-Propagation...\n')

# Check gradients by running check_nn_gradients(...)
check_nn_gradients()

input('Program paused. Press enter to continue.\n')

# ====================== Part 8: Implement Regularization =====================
#  Once your backpropagation implementation is correct, you should now continue
#  to implement the regularization with the cost and gradient.
#

print('Checking Back-Propagation (w/ Regularization)...')

# Check gradients by running check_nn_gradients(...)
rlambda = 3
check_nn_gradients(rlambda)

# Also output the cost_function debugging values
debug_J, _ = nn_cost_function(nn_params, input_layer_size,
                              hidden_layer_size, num_labels, X, y, rlambda)

print('Cost at (fixed) debugging parameters (w/ rlambda = 3) {0:.6f}\n'
      '(This value should be about 0.576051)\n'.format(debug_J))

input('Program paused. Press enter to continue.\n')

# ====================== Part 8: Training NN ==================================
#  You have now implemented all the code necessary to train a neural network.
#  To train your neural network, we will now use "fmin_cg", which is a function
#  which works similarly to "fmin_tnc". Recall that these advanced optimizers
#  are able to train our cost functions efficiently as long as we provide them
#  with the gradient computations.
#

print('Training Neural Network...')

#  You should try different values of rlambda
rlambda = 1

#  After you have completed the assignment, change the MaxIter to a larger
#  value to see how more training helps.
nn_params = spo.fmin_cg(
                f=lambda nn, X, y, l:
                    nn_cost_function(nn, input_layer_size, hidden_layer_size,
                                     num_labels, X, y, l)[0],
                fprime=lambda nn, X, y, l:
                    nn_cost_function(nn, input_layer_size,  hidden_layer_size,
                                     num_labels, X, y, l)[1].ravel(),
                x0=init_nn_params, args=(X, y, rlambda), maxiter=50)

# Obtain Theta1 and Theta2 back from nn_params
split = hidden_layer_size * (input_layer_size + 1)
Theta1 = nn_params[:split].reshape([hidden_layer_size, input_layer_size + 1])
Theta2 = nn_params[split:].reshape([num_labels, hidden_layer_size + 1])

input('\nProgram paused. Press enter to continue.\n')

# ====================== Part 9: Visualize Weights ============================
#  You can now "visualize" what the neural network is learning by displaying
#  the hidden units to see what features they are capturing in the data.
#

print('Visualizing Neural Network...\n')

display_data(Theta1[:, 1:])

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ====================== Part 10: Implement Predict ===========================
#  After training the neural network, we would like to use it to predict the
#  labels. You will now implement the "predict" function to use the neural
#  network to predict the labels of the training set. This lets you compute
#  the training set accuracy.
#

correct = np.where(predict(Theta1, Theta2, X) == y)
accuracy = float(correct[0].size / y.size)
print('Training set accuracy: {0:.2%}\n'.format(accuracy))

input('Program paused. Press enter to continue.\n')

# ====================== Part 11: Better Visualization ========================
#  You can now "visualize" what is the best input to trigger neural network
#  to detect a particular number. Error is back-propagated all the way to the
#  input image and gradient descent algorithm then iteratively updates the
#  input. Regularization ensures that the resulting image is simple and doesn't
#  contain artifacts never seen in the training set.
#

print('Visualizing Neural Network Even Better...\n')

xx = np.zeros([num_labels, input_layer_size])
yy = np.eye(num_labels)
xlambda, xalpha, steps = 0.01, 0.1, 100

for _ in range(steps):
    # Feed-Forward
    a1 = np.hstack([np.ones([num_labels, 1]), xx])
    z2 = a1 @ Theta1.T
    a2 = np.hstack([np.ones([num_labels, 1]), sigmoid(z2)])
    z3 = a2 @ Theta2.T
    a3 = sigmoid(z3)

    # Back-Propagation
    delta3 = (a3 - yy) * sigmoid_gradient(z3)
    delta2 = delta3 @ Theta2
    delta2 = delta2[:, 1:] * sigmoid_gradient(z2)
    delta1 = delta2 @ Theta1
    delta1 = delta1[:, 1:]

    # Regularized Gradient Descent Update
    gradient_xx = delta1 + (xx * xlambda / num_labels)
    xx -= xalpha * gradient_xx

display_data(xx)

print('Program paused. Close the plot window to continue.\n')
plt.show()
