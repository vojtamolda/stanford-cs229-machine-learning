import numpy as np
from sigmoid import sigmoid


def predict(Theta1, Theta2, X):
    """Predict the label of an input given the trained weights of a neural
     network (Theta1, Theta2)
     """

    m, n = X.shape

    a1 = np.hstack([np.ones([m, 1]), X])

    z2 = a1 @ Theta1.T
    a2 = np.hstack([np.ones([m, 1]), sigmoid(z2)])

    z3 = a2 @ Theta2.T
    a3 = sigmoid(z3)

    p = np.argmax(a3, axis=1).reshape([m, 1]) + 1

    return p
