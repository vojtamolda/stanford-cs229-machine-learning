import numpy as np


def compute_numerical_gradient(func, x0, args):
    """Computes the numerical gradient of the function J around theta.
     Calling y = func(theta, *args) should return the function value at theta.

     The following code implements numerical gradient checking, and returns
     the numerical gradient. It sets numgrad[i] to (a numerical
     approximation of) the partial derivative of func with respect to the
     i-th input argument, evaluated at theta. (i.e., numgrad[i] should
     be the (approximately) the partial derivative of J with respect to
     theta[i].)
     """

    epsilon = 1e-4
    numgrad = np.zeros(x0.shape)
    perturb = np.zeros(x0.shape)

    for i in range(x0.size):
        # Set perturbation vector
        perturb[i] = epsilon
        loss1 = func(x0 - perturb, *args)
        loss2 = func(x0 + perturb, *args)

        # Compute Numerical Gradient
        numgrad[i] = (loss2 - loss1) / (2 * epsilon)
        perturb[i] = 0

    return numgrad
