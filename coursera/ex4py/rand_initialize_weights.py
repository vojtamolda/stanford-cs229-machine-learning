import math
import numpy as np


def rand_initialize_weights(l_in, l_out):
    """Randomly initialize the weights of a layer with l_in incoming
     connections and l_out outgoing connections

     Note that w should be set to a matrix of size(L_out, 1 + L_in) as the
     column row of w handles the "bias" terms
     """

    # You need to return the following variables correctly
    w = np.zeros([l_out, l_in + 1])

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Initialize w randomly so that we break the symmetry while
    #               training the neural network.
    #
    # Note: The first row of w corresponds to the parameters for the bias units
    #

    eps = math.sqrt(6) / math.sqrt(l_in + l_out)
    w = np.random.uniform(-eps, +eps, [l_out, l_in + 1])

    # =========================================================================

    return w
