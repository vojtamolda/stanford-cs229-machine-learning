import numpy as np
from nn_cost_function import nn_cost_function
from compute_numerical_gradient import compute_numerical_gradient


def check_nn_gradients(rlambda=0):
    """Creates a small neural network to check the back-propagation gradients,
     it will output the analytical gradients produced by your back-propagation
     code and the numerical gradients (computed using compute_num_gradient).
     These two gradient computations should result in very similar values.
     """

    input_layer_size = 3
    hidden_layer_size = 5
    num_labels = 3
    m = 5

    # We generate some 'random' test data.
    Theta1 = debug_initialize_weights(hidden_layer_size, input_layer_size)
    Theta2 = debug_initialize_weights(num_labels, hidden_layer_size)

    # Reusing debug_initialize_weights to generate X.
    X = debug_initialize_weights(m, input_layer_size - 1)
    y = 1 + np.mod(np.arange(m), num_labels).reshape([m, 1])

    # Unroll parameters
    nn_params = np.concatenate([Theta1.ravel(), Theta2.ravel()])

    numgrad = compute_numerical_gradient(
            func=lambda nn, X, y, l:
                nn_cost_function(nn, input_layer_size, hidden_layer_size,
                                 num_labels, X, y, l)[0],
            x0=nn_params, args=(X, y, rlambda))
    _, grad = nn_cost_function(nn_params, input_layer_size, hidden_layer_size,
                               num_labels, X, y, rlambda)

    print(grad, numgrad, sep='\n')
    print('The above two vector you get should be very similar.\n'
          '(Higher-Numerical Gradient, Lower-Analytical Gradient)\n')

    error = np.linalg.norm(numgrad - grad) / np.linalg.norm(numgrad + grad)
    print('If your backpropagation implementation is correct, then difference\n'
          'will be small (less than 1e-9).\n'
          'Relative Difference: {}\n'.format(error))


def debug_initialize_weights(fan_out, fan_in):
    """Initialize the weights of a layer with fan_in incoming connections and
     fan_out outgoing connections using a fixed strategy, this will help you
     later in debugging.

     Note that w should be set to a matrix of size(1 + fan_in, fan_out) as the
     first row of w handles the "bias" terms.
     """

    # Set w to zeros
    w = np.zeros([fan_out, 1 + fan_in])

    # Initialize w using "sin", this ensures that w is always of the same
    # values and will be useful for debugging
    w = np.sin(np.arange(w.size)).reshape(w.shape) / 10

    return w
