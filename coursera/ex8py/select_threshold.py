import numpy as np


def select_threshold(yval, pval):
    """Find the best threshold (epsilon) to use for selecting outliers based
     on the results from a validation set (pval) and the ground truth (yval).
     """

    steps = 1000
    epsilons = np.linspace(pval.min(), pval.max(), steps)

    accuracy_plot = np.ones(epsilons.shape)
    precision_plot = np.ones(epsilons.shape)
    recall_plot = np.ones(epsilons.shape)
    f1score_plot = np.ones(epsilons.shape)

    for i, epsilon in enumerate(epsilons):
        # =================== YOUR CODE HERE ==================================
        # Instructions: Compute the F1 score of choosing epsilon as the
        #               threshold and place the value in F1. The code at the
        #               end of the loop will compare the F1 score for this
        #               choice of epsilon and set it to be the best epsilon if
        #               it is better than the current choice of epsilon.
        #
        # Note: You can use predictions = (pval < epsilon) to get a bool vector
        #       of Falses and Trues of the outlier predictions.

        normal = (pval >= epsilon)
        anomalous = (pval < epsilon)

        true_positives = ((yval == 1) * anomalous).sum()
        true_negatives = ((yval == 0) * normal).sum()
        false_positives = ((yval == 0) * anomalous).sum()
        false_negatives = ((yval == 1) * normal).sum()

        accuracy_plot[i] = (true_positives + true_negatives) / yval.size
        precision_plot[i] = true_positives / (true_positives + false_positives)
        recall_plot[i] = true_positives / (true_positives + false_negatives)
        f1score_plot[i] = 2 * precision_plot[i] * recall_plot[i] \
                          / (precision_plot[i] + recall_plot[i])

        # =====================================================================

    # import matplotlib.pyplot as plt
    #
    # plt.figure()
    # plt.hold(True)
    # plt.plot(epsilons, accuracy_plot, 'r')
    # plt.plot(epsilons, precision_plot, 'm')
    # plt.plot(epsilons, recall_plot, 'c')
    # plt.plot(epsilons, f1score_plot, 'b')
    # plt.title('Performance Metrics')
    # plt.xlabel('epsilon')
    # plt.legend(['Accuracy', 'Precision', 'Recall', 'F1-Score'])
    # plt.hold(False)

    idx = np.argmax(np.nan_to_num(f1score_plot))
    best_f1score = f1score_plot[idx]
    best_epsilon = epsilons[idx]

    return best_epsilon, best_f1score
