# Machine Learning Online Class
#  Exercise 8 - Collaborative Filtering
#
#  Instructions
#  ------------
#
#  This file contains code that helps you get started on the exercise. You
#  will need to complete the following functions:
#
#     cofi_cost_func.py
#
#  For this exercise, you will not need to change any code in this file, or any
#  other files other than those mentioned above.
#

import numpy as np
import scipy.io as scio
import scipy.optimize as spo
import matplotlib.pyplot as plt
from cofi_cost_func import cofi_cost_func
from check_cost_gradients import check_cost_gradients
from normalze_ratings import normalize_ratings
from get_movie_list import get_movie_list

# ============ Part 1: Loading movie ratings dataset ==========================
#  You will start by loading the movie ratings dataset to understand the
#  structure of the data.
#

print('Loading Movie Ratings Dataset...')

# Y is a 1682x943 matrix, containing ratings (1-5) of 1682 movies on 943 users
# R is a 1682x943 matrix, where R(i,j) = 1 if and only if user j rated movie i
data = scio.loadmat('../ex8/ex8_movies.mat')
Y, R = data['Y'], data['R']

#  From the matrix, we can compute statistics like average rating.
movie0rtng = Y[0, R[0, :]].mean()
print('Average rating for movie 0 (Toy Story): {:.1f}/5\n'.format(movie0rtng))

# We can "visualize" the ratings matrix by plotting it with imshow
plt.imshow(Y)
plt.ylabel('Movies')
plt.xlabel('Users')

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ============ Part 2: Collaborative Filtering Cost Function ==================
#  You will now implement the cost function for collaborative filtering. To
#  help you debug your cost function, we have included set of weights that we
#  trained on that. Specifically, you should complete the code in
#  cofi_cost_func.py to return J.

print('Checking Cost Function (without regularization)...')

# Load pre-trained weights
data = scio.loadmat('../ex8/ex8_movieParams.mat')
num_users, num_movies = data['num_users'], data['num_movies']
num_features = data['num_features']
X, Theta = data['X'], data['Theta']

# Reduce the data set size so that this runs faster
num_users, num_movies, num_features = 4, 5, 3
X = X[:num_movies, :num_features]
Theta = Theta[:num_users, :num_features]
Y = Y[:num_movies, :num_users]
R = R[:num_movies, :num_users]

# Evaluate cost function
J, _ = cofi_cost_func(np.concatenate([X.ravel(), Theta.ravel()]), Y, R,
                      num_users, num_movies, num_features, 0)

print('Cost at loaded parameters: {:.4f}'.format(J))
print('(this value should be about 22.22)\n')

input('Program paused. Press enter to continue.\n')

# ============ Part 3: Collaborative Filtering Gradient =======================
#  Once your cost function matches up with ours, you should now implement the
#  collaborative filtering gradient function. Specifically, you should complete
#  the code in cofi_cost_func.py to return the grad argument.
#

print('Checking Gradients (without regularization)...')

# Check gradients
check_cost_gradients()

input('Program paused. Press enter to continue.\n')

# ============ Part 4: Collaborative Filtering Cost Regularization ============
#  Now, you should implement regularization for the cost function for 
#  collaborative filtering. You can implement it by adding the cost of
#  regularization to the original cost computation.
#  

print('Checking Cost Function (with regularization)...')

# Evaluate cost function
J, _ = cofi_cost_func(np.concatenate([X.ravel(), Theta.ravel()]), Y, R,
                      num_users, num_movies, num_features, 1.5)

print('Cost at loaded parameters (lambda=1.5): {:.4f}'.format(J))
print('(this value should be about 31.34)\n')

input('Program paused. Press enter to continue.\n')

# ============ Part 5: Collaborative Filtering Gradient Regularization ========
#  Once your cost matches up with ours, you should proceed to implement 
#  regularization for the gradient. 
#

print('\nChecking Gradients (with regularization) ... \n')

# Check gradients with regularization
check_cost_gradients(1.5)

input('Program paused. Press enter to continue.\n')

# ============ Part 6: Entering ratings for a new user ========================
#  Before we will train the collaborative filtering model, we will first add
#  ratings that correspond to a new user that we just observed. This part of
#  the code will also allow you to put in your own ratings for the movies in
#  our dataset!
#

print('Setting new user ratings...')

# Load movie list
movie_list = get_movie_list()

# Initialize my ratings
my_ratings = np.zeros([1682, 1])

# Check the file movie_idx.txt for id of each movie in our dataset
# For example, Toy Story (1995) has ID 0, so to rate it "4", you can set
my_ratings[0] = 4

# Or suppose did not enjoy Silence of the Lambs (1991), you can set
my_ratings[97] = 2

# We have selected a few movies we liked / did not like:
my_ratings[6] = 3
my_ratings[11] = 5
my_ratings[53] = 4
my_ratings[63] = 5
my_ratings[65] = 3
my_ratings[68] = 5
my_ratings[182] = 4
my_ratings[225] = 5
my_ratings[354] = 5

print('New user ratings:')
for i, rating in enumerate(my_ratings):
    if rating > 0:
        print('Rated {:.1f}/5 for {}'.format(my_ratings[i, 0], movie_list[i]))

input('\nProgram paused. Press enter to continue.\n')


# ============ Part 7: Learning Movie Ratings =================================
#  Now, you will train the collaborative filtering model on a movie rating
#  dataset of 1682 movies and 943 users.
#

print('Training collaborative filtering...')

# Load data
# Y is a 1682x943 matrix, containing ratings (1-5) of 1682 movies on 943 users
# R is a 1682x943 matrix, where R(i,j) = 1 if and only if user j rated movie i
data = scio.loadmat('../ex8/ex8_movies.mat')
Y, R = data['Y'], data['R']

# Add our own ratings to the data matrix
Y = np.hstack([my_ratings, Y])
R = np.hstack([(my_ratings != 0), R])

# Normalize Ratings
Ynorm, Ymean = normalize_ratings(Y)

# Useful Values
num_users = Y.shape[1]
num_movies = Y.shape[0]
num_features = 10

# Set Initial Parameters (Theta, X)
X = np.random.randn(num_movies, num_features)
Theta = np.random.randn(num_users, num_features)
init_theta = np.concatenate([X.ravel(), Theta.ravel()])

# Set Regularization
rlambda = 10
theta = spo.fmin_cg(f=lambda t, Y, R, nu, nm, nf, l:
                        cofi_cost_func(t, Y, R, nu, nm, nf, l)[0],
                    fprime=lambda t, Y, R, nu, nm, nf, l:
                        cofi_cost_func(t, Y, R, nu, nm, nf, l)[1],
                    args=(Ynorm, R, num_users, num_movies, num_features, rlambda),
                    x0=init_theta, maxiter=100, disp=False)

# Unfold the returned theta back into X and Theta
X = theta[:num_movies * num_features].reshape([num_movies, num_features])
Theta = theta[num_movies * num_features:].reshape([num_users, num_features])

print('Recommender system learning completed.\n')

input('Program paused. Press enter to continue.\n')

# ============ Part 8: Recommendation for you =================================
#  After training the model, you can now make recommendations by computing the
#  predictions matrix.
#

print('Calculating Predicted Ratings...')

p = X @ Theta.T
my_predictions = p[:, [0]] + Ymean
my_best_idx = np.argsort(my_predictions, axis=0)[::-1]

print('\nTop recommendations for you:')
for i in my_best_idx[:11, 0]:
    print('Predicted {:.1f}/5 for {}'.
          format(my_predictions[i, 0], movie_list[i]))

print('\nPredictions on provided ratings:')
for i, rating in enumerate(my_ratings):
    if rating > 0:
        print('Predicted {:.1f}/{:.1f} for {}'.
              format(my_predictions[i, 0], my_ratings[i, 0], movie_list[i]))
