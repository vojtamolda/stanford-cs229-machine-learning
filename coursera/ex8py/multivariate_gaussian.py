import numpy as np


def multivariate_gaussian(X, mu, Sigma2):
    """Computes the probability density function of the examples X under the
     multivariate gaussian distribution with parameters mu and Sigma2. If Sigma2
     is a matrix, it is treated as the covariance matrix. If Sigma2 is a
     vector, it is treated as the \sigma^2 values of the variances in each
     dimension (a diagonal covariance matrix).
     """

    k = mu.size

    if Sigma2.shape[0] == 1 or Sigma2.shape[1] == 1:
        Sigma2 = np.diag(Sigma2.ravel())

    Xp = X - mu
    p = np.exp(-(Xp @ np.linalg.pinv(Sigma2) * Xp).sum(axis=1, keepdims=1) / 2)\
        / np.sqrt((2 * np.pi)**k * np.linalg.det(Sigma2))

    return p
