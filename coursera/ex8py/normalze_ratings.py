import numpy as np


def normalize_ratings(Y):
    """Preprocess data by subtracting mean rating for every movie (every row).
     Normalize Y so that each movie has a rating of 0 on average, and returns
     the mean rating in Ymean.
     """

    Ymean = Y.mean(axis=1, keepdims=True)
    Ynorm = Y - Ymean

    return Ynorm, Ymean
