import numpy as np
import matplotlib.pyplot as plt
from multivariate_gaussian import multivariate_gaussian


def visualize_fit(X, mu, sigma2):
    """This visualization shows you the probability density function of the
     Gaussian distribution. Each example has a location (x1, x2) that depends
     on its feature values.
     """

    x1, x2 = np.meshgrid(np.arange(0, 35, 0.5), np.arange(0, 35, 0.5))
    z = multivariate_gaussian(np.vstack([x1.ravel(), x2.ravel()]).T, mu, sigma2)
    z = z.reshape(x1.shape)

    plt.figure()
    plt.hold(True)
    plt.plot(X[:, 0], X[:, 1], 'bx')
    contours = 10 ** np.arange(-20.0, 0.0, 3.0)
    plt.contour(x1, x2, z, contours)
    plt.hold(False)
