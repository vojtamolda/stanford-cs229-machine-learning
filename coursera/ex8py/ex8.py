# Machine Learning Online Class
#  Exercise 8 - Anomaly Detection
#
#  Instructions
#  ------------
#
#  This file contains code that helps you get started on the exercise. You 
#  will need to complete the following functions:
#
#     estimate_gaussian.py
#     select_threshold.py
#
#  For this exercise, you will not need to change any code in this file, or any
#  other files other than those mentioned above.
#

import numpy as np
import scipy.io as scio
import matplotlib.pyplot as plt
from estimate_gaussian import estimate_gaussian
from multivariate_gaussian import multivariate_gaussian
from select_threshold import select_threshold
from visualize_fit import visualize_fit

# ================= Part 1: Load Example Dataset ==============================
#  We start this exercise by using a small dataset that is easy to visualize.
#
#  Our example case consists of 2 network server statistics across several
#  machines: the latency and throughput of each machine. This exercise will
#  help us find possibly faulty (or very fast) machines.
#

print('Visualizing Example Dataset for Outlier Detection...\n')

# Load the example dataset
data = scio.loadmat('../ex8/ex8data1.mat')
Xval, yval = data['Xval'], data['yval']
X = data['X']

#  Visualize the example dataset
plt.plot(X[:, 0], X[:, 1], 'bx')
plt.axis([0, 30, 0, 30])
plt.xlabel('Latency [ms]')
plt.ylabel('Throughput [mb/s]')

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ================= Part 2: Estimate the dataset statistics ===================
#  For this exercise, we assume a Gaussian distribution for the dataset.
#
#  We first estimate the parameters of our assumed Gaussian distribution, then
#  compute the probabilities for each of the points and then visualize both
#  the overall distribution and where each of the points falls in terms of that
#  distribution.
#

print('Visualizing Gaussian Fit...\n')

# Estimate my and sigma2
mu, sigma2 = estimate_gaussian(X)

# Calculate the probability density of the multivariate normal at each data
# point (row) of train dataset X
p = multivariate_gaussian(X, mu, sigma2)

#  Visualize the fit
visualize_fit(X, mu, sigma2)
plt.xlabel('Latency [ms]')
plt.ylabel('Throughput [mb/s]')

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ================= Part 3: Find Outliers =====================================
#  Now you will find a good epsilon threshold using a cross-validation set
#  probabilities given the estimated Gaussian distribution.
#

print('Finding Outliers...')
# Calculate the probability density of the multivariate normal at each data
# point (row) of cross validation dataset Xval
pval = multivariate_gaussian(Xval, mu, sigma2)

#  Find the best threshold
epsilon, f1score = select_threshold(yval, pval)

print('Best epsilon found using cross-validation: {:.2e}'.format(epsilon))
print('Best F1 score on cross validation set:  {:.3f}'.format(f1score))
print('(you should see a value epsilon of about 8.99e-05)\n')

# Find the outliers in the training set and plot the
outliers = np.where(p < epsilon)

# Draw a red circle around those outliers
visualize_fit(X, mu, sigma2)
plt.xlabel('Latency [ms]')
plt.ylabel('Throughput [mb/s]')
plt.hold(True)
plt.plot(X[outliers[0], 0], X[outliers[0], 1], 'ro', linewidth=2, markersize=10)
plt.hold(False)

print('Program paused. Close the plot window to continue.\n')
plt.show()

# ================= Part 4: Multidimensional Outliers =========================
#  We will now use the code from the previous part and apply it to a harder
#  problem in which more features describe each datapoint and only some
#  features indicate whether a point is an outlier.
#

print('Loading Multidimensional Dataset...')

# Load the second dataset
data = scio.loadmat('../ex8/ex8data2.mat')
Xval, yval = data['Xval'], data['yval']
X = data['X']

# Apply the same steps to the larger dataset
mu, sigma2 = estimate_gaussian(X)

# Training set
p = multivariate_gaussian(X, mu, sigma2)

# Cross-validation set
pval = multivariate_gaussian(Xval, mu, sigma2)

#  Find the best threshold
epsilon, f1score = select_threshold(yval, pval)

# Find the outliers in the training set and count them
outliers = (p < epsilon).sum()

print('Best epsilon found using cross-validation: {:.2e}'.format(epsilon))
print('Best F1 score on cross validation set:  {:.3f}'.format(f1score))
print('Number of outliers found: {}'.format(outliers))
print('(you should see a value epsilon of about 1.38e-18)\n')
