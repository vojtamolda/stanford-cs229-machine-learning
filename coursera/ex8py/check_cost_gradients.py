import numpy as np
from cofi_cost_func import cofi_cost_func
from compute_numerical_gradient import compute_numerical_gradient


def check_cost_gradients(rlambda=0):
    """Creates a collaborative filering problem to check your cost function and
     gradients, it will output the analytical gradients produced by your code
     and the numerical gradients (computed using compute_numerical_gradient).
     These two gradient computations should result in very similar values.
     """

    # Create small problem
    X_t = np.random.rand(4, 3)
    Theta_t = np.random.rand(5, 3)

    # Zap out most entries
    Y = X_t @ Theta_t.T
    Y[np.where(np.random.rand(Y.shape[0], Y.shape[1]) > 0.5)] = 0
    R = np.zeros(Y.shape)
    R[np.where(Y != 0)] = 1

    # Run Gradient Checking
    X = np.random.randn(X_t.shape[0], X_t.shape[1])
    Theta = np.random.randn(Theta_t.size)
    num_users = Y.shape[1]
    num_movies = Y.shape[0]
    num_features = Theta_t.shape[1]

    numgrad = compute_numerical_gradient(
                    func=lambda t, Y, R, nu, nm, nf, l:
                            cofi_cost_func(t, Y, R, nu, nm, nf, l)[0],
                    x0=np.concatenate([X.ravel(), Theta.ravel()]),
                    args=(Y, R, num_users, num_movies, num_features, rlambda))
    _, grad = cofi_cost_func(np.concatenate([X.ravel(), Theta.ravel()]), Y, R,
                             num_users, num_movies, num_features, rlambda)

    print(grad, numgrad, sep='\n')
    print('The above two vector you get should be very similar.\n'
          '(Higher-Numerical Gradient, Lower-Analytical Gradient)\n')

    error = np.linalg.norm(numgrad - grad) / np.linalg.norm(numgrad + grad)
    print('If your gradient implementation is correct, then difference\n'
          'will be small (less than 1e-9).\n'
          'Relative Difference: {}\n'.format(error))
