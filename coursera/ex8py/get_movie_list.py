

def get_movie_list():
    """Reads the fixed movie list in movie_idx.txt and returns a list mapping
     ids to movie names.
     """

    # Create a dictionary
    movie_list = []

    # Open the movie ids file
    with open('../ex8/movie_ids.txt', encoding='ISO-8859-1') as file:
        # Read lines into the list
        for line in file:
            words = line.split()
            movie_list.insert(int(words[0]), ' '.join(words[1:]))
        file.close()

    return movie_list
