# Machine Learning Online Class
#  Exercise 1: Linear regression with multiple variables
#
#  Instructions
#  ------------
# 
#  This file contains code that helps you get started on the
#  linear regression exercise. 
#
#  You will need to complete the following functions in this 
#  exericse:
#
#     gradient_descent_multi.py
#     compute_cost_multi.py
#     feature_normalize.py
#     normal_eqn.py
#
#  For this part of the exercise, you will need to change some
#  parts of the code below for various experiments (e.g., changing
#  learning rates).
#

import numpy as np
import matplotlib.pyplot as plt
from feature_normalize import feature_normalize
from gradient_descent_multi import gradient_descent_multi
from normal_eqn import normal_eqn

# =========================== Part 1: Feature Normalization ===================
print('Loading data...')
data = np.loadtxt('../ex1/ex1data2.txt', delimiter=',')
m = data.shape[0]
X = np.array(data[:, 0:2]).reshape([m, 2])
y = np.array(data[:, 2]).reshape([m, 1])

# Print out some data points
print('First 10 examples from the dataset: ')
print(data[1:10, :])
print()

# Scale features and set them to zero mean
print('\nNormalizing Features...')
X, mu, sigma = feature_normalize(X)

# Add intercept term to X
X = np.hstack([np.ones([m, 1]), X])
n = X.shape[1]

# =========================== Part 2: Gradient Descent ========================

# =========================== YOUR CODE HERE ==================================
# Instructions: We have provided you with the following starter code that
#               runs gradient descent with a particular learning rate (alpha).
#
#               Your task is to first make sure that your functions -
#               compute_cost(...) and gradient_descent(...) already work with
#               this starter code and support multiple variables.
#
#               After that, try running gradient descent with different values
#               of alpha and see which one gives you the best result.
#
#               Finally, you should complete the code at the end to predict the
#               price of a 1650 sq-ft, 3 br house.
#
# Hint: By using the 'hold(True)' command, you can plot multiple graphs on
#       the same figure.
#
# Hint: At prediction, make sure you do the same feature normalization.
#

print('Running gradient descent...')

# Choose some alpha value
alpha = 0.01
num_iters = 400

# Init theta and run gradient descent
init_theta = np.zeros([n, 1])
theta, J_history = gradient_descent_multi(X, y, init_theta, alpha, num_iters)

# Plot the convergence graph
plt.figure()
plt.plot(range(len(J_history)), J_history, '-b', linewidth=2)
plt.xlabel('Number of iterations')
plt.ylabel('Cost J')

# Display gradient descent's result
print('Theta computed from gradient descent: \n{}'.format(theta.T))

# =========================== YOUR CODE HERE ==================================
# Instructions: Estimate the price of a 1650 sq-ft, 3 br house. Recall that
#               the first column of X is all-ones. Thus, it does not need to
#               be normalized.
#

x = (np.array([[1650, 3]]) - mu) / sigma
x = np.hstack([[[1.0]], x])
price = int(x @ theta)

# =============================================================================

print('Predicted price of a 1650 sq-ft, 3 br house (using gradient descent): '
      ' ${0:,d}\n'.format(price))

print('Program paused. Close the plot windows to continue.\n')
plt.show()

# =========================== Part 3: Normal Equations ========================

print('Loading data...')
# Load Data
data = np.loadtxt('../ex1/ex1data2.txt', delimiter=',')
m = data.shape[0]
X = np.array(data[:, 0:2]).reshape([m, 2])
y = np.array(data[:, 2]).reshape([m, 1])

# Add intercept term to X
X = np.hstack([np.ones([m, 1]), X])

# =========================== YOUR CODE HERE ==================================
# Instructions: The following code computes the closed form solution for linear
#               regression using the normal equations. You should complete the
#               code fpr normal_eqn(...) After doing so, you should complete
#               this code to predict the price of a 1650 sq-ft, 3 br house.
#

print('Solving with normal equations...')

# Normal equations
theta = normal_eqn(X, y)

# =============================================================================

# Display normal equation's result
print('Theta computed from the normal equations: \n{}'.format(theta.T))

# =========================== YOUR CODE HERE ==================================
# Instructions: Estimate the price of a 1650 sq-ft, 3 br house.
#

x = np.array([[1, 1650, 3]])
price = int(x @ theta)

# =============================================================================

print('Predicted price of a 1650 sq-ft, 3 br house (using normal equations): '
      ' ${0:,d}\n'.format(price))
