# Machine Learning Online Class
#  Exercise 1: Linear Regression
#
#  Instructions
#  ------------
# 
#  This file contains code that helps you get started on the
#  linear exercise. You will need to complete the following functions 
#  in this exericse:
#
#     warm_up_exercise.py
#     plot_data.py
#     gradient_descent.py
#     compute_cost.py
#
#  For this exercise, you will not need to change any code in this file,
#  or any other files other than those mentioned above.
#
# x refers to the population size in 10,000s
# y refers to the profit in $10,000s
#

import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d
from warm_up_exercise import warm_up_exercise
from plot_data import plot_data
from compute_cost import compute_cost
from gradient_descent import gradient_descent

# =========================== Part 1: Basic Function ==========================
print('Running warm up exercise... ')
print('5x5 Identity Matrix: \n{}\n'.format(warm_up_exercise()))

input('Program paused. Press enter to continue.\n')

# =========================== Part 2: Plotting ================================
print('Plotting Data...')
data = np.loadtxt('../ex1/ex1data1.txt', delimiter=',')
m = data.shape[0]
X = np.array(data[:, 0]).reshape([m, 1])
y = np.array(data[:, 1]).reshape([m, 1])

# Plot Data
plot_data(X, y)

# =========================== Part 3: Gradient descent ========================
print('Running Gradient Descent...')
X = np.hstack([np.ones([m, 1]), X])  # Add a column of ones to x
n = X.shape[1]

theta = np.zeros([n, 1])  # initialize fitting parameters

# Some gradient descent settings
iterations = 1500
alpha = 0.01

# Compute and display initial cost
compute_cost(X, y, theta)

# Run gradient descent
theta, _ = gradient_descent(X, y, theta, alpha, iterations)

# Print theta to screen
print('Theta found by gradient descent: \n{}'.format(theta.T))

# Plot the linear fit
plt.hold(True)  # keep previous plot visible
plt.plot(X[:, 1], X @ theta, '-')
plt.legend(['Training data', 'Linear regression'])
plt.hold(False)  # don't overlay any more plots on this figure

# Predict values for population sizes of 35,000 and 70,000
pred1 = int(np.array([1, 3.5]) @ theta * 10000)
print('For population = 35,000, we predict a profit of ${0:,d}'.format(pred1))
pred2 = int(np.array([1, 7.0]) @ theta * 10000)
print('For population = 70,000, we predict a profit of ${0:,d}\n'.format(pred2))

print('Program paused. Close the plot window to continue.\n')
plt.show()

# =========================== Part 4: Visualizing J ===========================
print('Visualizing J(theta_0, theta_1)...\n')

# Grid over which we will calculate J
theta0_vals = np.linspace(-10, 10, 100)
theta1_vals = np.linspace(-1, 4, 100)

# Initialize J_vals to a matrix of 0's
J_vals = np.zeros([len(theta0_vals), len(theta1_vals)])

# Fill out J_vals
for i, theta0_val in enumerate(theta0_vals):
    for j, theta1_val in enumerate(theta1_vals):
        theta_val = np.array([[theta0_val], [theta1_val]])
        J_vals[i, j] = compute_cost(X, y, theta_val)

# Because of the way meshgrids work in the surf command, we need to
# transpose J_vals before calling surf, or else the axes will be flipped
J_vals = J_vals.T

# Surface plot
ax = plt.figure().gca(projection='3d')
plt.hold(True)
xa, ya = np.meshgrid(theta0_vals, theta1_vals)
ax.plot_surface(xa, ya, J_vals, cmap='jet')
plt.xlabel('theta_0')
plt.ylabel('theta_1')

ax.plot(theta[0], theta[1], compute_cost(X, y, theta), 'rx',
        markersize=10, linewidth=5)
plt.show(block=False)
plt.hold(False)

# Contour plot
plt.figure()
plt.hold(True)
# Plot J_vals as 15 contours spaced logarithmically between 0.01 and 100
plt.contour(theta0_vals, theta1_vals, J_vals, np.logspace(-2, 3, 20))
plt.xlabel('theta_0')
plt.ylabel('theta_1')
plt.plot(theta[0], theta[1], 'rx', markersize=10, linewidth=5)
plt.hold(False)

print('Program paused. Close all plot windows to continue.\n')
plt.show()
