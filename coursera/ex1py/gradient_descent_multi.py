import numpy as np
from compute_cost_multi import compute_cost_multi


def gradient_descent_multi(X, y, theta, alpha, num_iters):
    """Performs gradient descent to learn theta. theta is updated by taking
     num_iters gradient steps with learning rate alpha.
     """

    # Initialize some useful values
    m = X.shape[0]  # number of training examples
    J_history = np.zeros([num_iters, 1])  # history of cost function

    for iter in range(num_iters):

        # =================== YOUR CODE HERE ==================================
        # Instructions: Perform a single gradient step on the parameter vector
        #               theta.
        #
        # Hint: While debugging, it can be useful to print out the values of
        #       the cost function compute_cost(...) and gradient here.
        #

        J_grad = X.T @ (X @ theta - y) / m
        theta -= alpha * J_grad

        # =====================================================================

        J_history[iter] = compute_cost_multi(X, y, theta)  # Save the cost

    # You need to return these values correctly
    return theta, J_history
