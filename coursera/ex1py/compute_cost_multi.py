def compute_cost_multi(X, y, theta):
    """Compute the cost of using theta as the parameter for linear regression
     with multiple variables to fit the data points in X and y.
     """

    # Initialize some useful values
    m = X.shape[0]  # number of training examples
    J = 0  # cost function

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Compute the cost of a particular choice of theta.
    #               You should set J to the cost.

    J = ((X @ theta - y).T @ (X @ theta - y)) / 2 / m

    # =========================================================================

    # You need to set this value correctly
    return float(J)
