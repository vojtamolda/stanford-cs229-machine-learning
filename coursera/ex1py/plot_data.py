import matplotlib.pyplot as plt


def plot_data(X, y):
    """Plots the data points and gives the figure axes labels of population and
     profit.
     """

    # ======================= YOUR CODE HERE ==================================
    # Instructions: Plot the training data into a figure using the "figure" and
    #               "plot" commands. Set the axes labels using the "xlabel" and
    #               "ylabel" commands. Assume the population and revenue data
    #               have been passed in as the x and y arguments of this
    #               function.
    #
    # Hint: You can use the 'rx' option with plot to have the markers appear as
    #       red crosses. Furthermore, you can make the markers larger by using
    #       plot(..., 'rx', markersize=10);

    plt.figure()  # open a new figure window

    plt.plot(X, y, 'rx', markersize=10)
    plt.ylabel('Profit [$10k]')
    plt.xlabel('City Population [10k]')

    # =========================================================================
