import numpy as np


def normal_eqn(X, y):
    """Computes the closed-form solution to linear regression using the
     normal equations.
     """

    theta = np.zeros([3, 1])

    # ======================= YOUR CODE HERE ===================================
    # Instructions: Complete the code to compute the closed form solution to
    #               linear regression and put the result in theta.
    #

    theta = np.linalg.pinv(X.T @ X) @ X.T @ y

    # ==========================================================================

    return theta
