function [q, steps_per_episode] = qlearning(episodes)

% set up parameters and initialize q values
alpha = 0.05;
gamma = 0.99;
num_states = 100;
num_actions = 2;
actions = [-1, 1];
q = zeros(num_states, num_actions);
steps_per_episode = zeros(1, episodes);

% iterate over episodes
for episode = 1:episodes
  [x, s, absorb] = mountain_car([0.0 -pi/6], 0);
  a = greedy_policy(q(s, :));
  
  % iterate while not on top of the hill
  while absorb != 1
    [x_next, s_next, absorb] = mountain_car(x, actions(a));
    [a_next, greedy_q] = greedy_policy(q(s_next, :));
    reward = absorb - 1;
    q(s, a) = (1 - alpha)*q(s, a) + alpha*(reward + gamma*greedy_q);

    steps_per_episode(episode) = steps_per_episode(episode) + 1;
    x = x_next;
    a = a_next;
    s = s_next;
  end
end


function [a, greedy_q] = greedy_policy(choices_q)

if (range(choices_q) > 0)
  [greedy_q, a] = max(choices_q);
else
  greedy_q = 0;
  a = randi(size(choices_q));
end
