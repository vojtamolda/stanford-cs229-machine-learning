function anim_mountain_car(q)

alpha = 0.05;
gamma = 0.99;
num_states = 100;
num_actions = 2;
actions = [-1, 1];
q = zeros(num_states, num_actions);

step = 1;
f = figure;
[x, s, absorb] = mountain_car([0.0 -pi/6], 0);
while absorb != 1
  [max_q, a] = max(q(s, :));
  [x, s, absorb] = mountain_car(x, a);
  plot_mountain_car(x);
  drawnow;
  pause(0.5);
  saveas(f, sprintf('anim_mountain_car-%d', step), 'png')
  step = step + 1;
end
