function U = pca(X)

Sigma = X * X';
[U, ~, ~] = svd(Sigma);

end
