function W = ica(X)

batch = 100;
alpha = 5e-4;
[n, m] = size(X);
W = rand(n);

for dataPass = 1:10
    perm = randperm(m);
    for i = 1:batch:m-batch+1
        Xb = X(:, perm(i:i+batch-1));
        gradW = (1 - 2*sigmoid(W * Xb)) * Xb' + batch * pinv(W');
        W = W + alpha * gradW;
    end
end

end
