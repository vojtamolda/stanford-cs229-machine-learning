import math
import numpy as np
import plotly as ply
import plotly.graph_objs as plygo


def plot_digits(x, predicted=None, target=None, title='', filename='temp.html'):
    rows = round(math.sqrt(x.shape[0]))
    cols = math.ceil(x.shape[0] / rows)
    figure = ply.tools.make_subplots(rows=rows, cols=cols, print_grid=False)

    for row in range(rows):
        for col in range(cols):
            subplot = (cols * row) + col
            if subplot < x.shape[0]:
                image_size = round(math.sqrt(x[subplot, :].size))
                image = x[subplot, :].reshape([image_size, image_size])
                image = np.rot90(image, 3).T
                heatmap = plygo.Heatmap(z=image, colorscale='Greys', reversescale=True,
                                        showscale=False, hoverinfo='none')
                figure.append_trace(heatmap, row + 1, col + 1)
            if subplot < x.shape[0] and predicted is not None and target is not None:
                xtitle = '{} (Predicted {})'.format(target[subplot], predicted[subplot])
            else:
                xtitle = ''
            figure['layout']['xaxis' + str(subplot + 1)].update(title=xtitle,
                                          showgrid=False, showline=False, zeroline=False,
                                          ticks='', showticklabels=False, fixedrange=True)
            figure['layout']['yaxis' + str(subplot + 1)].update(
                                          showgrid=False, showline=False, zeroline=False,
                                          ticks='', showticklabels=False, fixedrange=True)


    figure['layout']['title'] = title

    ply.offline.plot(figure, filename=filename)


def plot_confusion_matrix(confusion_matrix, digits, filename='temp.html'):
    heatmap = plygo.Heatmap(x=str(digits.target_names), y=str(digits.target_names), z=confusion_matrix,
                            zmin=0, zmax=100, name='Confustion Matrix', transpose=True,
                            colorscale=[[0.0, '#fff'], [0.1, '#29f'], [0.9, '#29f'], [1.0, '#000']])
    layout = plygo.Layout(title='Confusion Matrix',
                          xaxis=dict(title='Predicted', showticklabels=True, tickangle=45, dtick=1,
                                     showgrid=True, showline=True),
                          yaxis=dict(title='Truth', showticklabels=True, dtick=1, showgrid=True))
    figure = plygo.Figure(data=[heatmap], layout=layout)
    ply.offline.plot(figure, filename=filename)


def plot_learning_curve(size, train, test, filename='temp.html'):
    train_mean = train.mean(axis=1)
    train_std = train.std(axis=1)
    test_mean = test.mean(axis=1)
    test_std = test.std(axis=1)

    train_mid = plygo.Scatter(name='Training', x=size, y=train_mean, line=dict(color='red'),
                              error_y=dict(type='data', array=train_std, color='red', thickness=1))

    test_mid = plygo.Scatter(name='Cross-Validation', x=size, y=test_mean, line=dict(color='green'),
                             error_y=dict(type='data', array=test_std, color='green', thickness=1))
    layout = plygo.Layout(title='Learning Curve',
                          xaxis=dict(title='Training Set Size'),
                          yaxis=dict(title='Accuracy', range=[0.5, 1.1]))
    figure = plygo.Figure(data=[train_mid, test_mid], layout=layout)
    ply.offline.plot(figure, filename=filename)


def plot_validation_curve(param, train, test, xtitle='', filename='temp.html'):
    train_mean = train.mean(axis=1)
    train_std = train.std(axis=1)
    test_mean = test.mean(axis=1)
    test_std = test.std(axis=1)

    train_mid = plygo.Scatter(name='Training', x=param, y=train_mean, line=dict(color='red'),
                              error_y=dict(type='data', array=train_std, color='red', thickness=1))

    test_mid = plygo.Scatter(name='Cross-Validation', x=param, y=test_mean, line=dict(color='green'),
                             error_y=dict(type='data', array=test_std, color='green', thickness=1))
    layout = plygo.Layout(title='Validation Curve',
                          xaxis=dict(title=xtitle, type='log'),
                          yaxis=dict(title='Accuracy', range=[0.0, 1.1]))
    figure = plygo.Figure(data=[train_mid, test_mid], layout=layout)
    ply.offline.plot(figure, filename=filename)


colorscale = {0: 'yellow', 1: 'pink', 2: 'red', 3: 'cyan', 4: 'green',
              5: 'blue', 6: 'magenta', 7: 'purple', 8: 'grey', 9: 'maroon'}

def plot_pca2d_classification(classifier, pca, digits, resolution=200, filename='temp.html'):
    data = pca.transform(digits.data)
    x_min, x_max = data[:, 0].min(), data[:, 0].max()
    xs = np.linspace(x_min, x_max, resolution)
    y_min, y_max = data[:, 1].min(), data[:, 1].max()
    ys = np.linspace(y_min, y_max, resolution)
    x, y = np.meshgrid(xs, ys)
    xy = np.c_[x.ravel(), y.ravel()]
    xy_boundary = pca.inverse_transform(xy)

    boundary = classifier.predict(xy_boundary).reshape(x.shape)
    predict = classifier.predict(digits.data)

    heatmap = plygo.Heatmap(name='Digit Classification Visualization',
                            showlegend=False,
                            x=xs,
                            y=ys,
                            z=boundary,
                            colorscale=list(map(lambda cls: [cls[0] / 9, cls[1]], colorscale.items())),
                            showscale=False, hoverinfo='z')
    charts = [heatmap]

    for digit in np.unique(digits.target):
        correct = np.where((digits.target == predict) * (digits.target == digit))[0]
        incorrect = np.where((digits.target != predict) * (digits.target == digit))[0]
        correct = plygo.Scatter(name='{}'.format(digit), mode='markers',
                                legendgroup='digit{}'.format(digit),
                                showlegend=True,
                                x=data[correct, 0],
                                y=data[correct, 1],
                                text=predict[correct], hoverinfo='text',
                                marker=dict(size=7, color=colorscale[digit],
                                            colorbar=dict(len=0.8),
                                            line=dict(width=1, color='black'),
                                            showscale=False))
        incorrect = plygo.Scatter(name='{}'.format(digit), mode='markers',
                                  legendgroup='digit{}'.format(digit),
                                  showlegend=False,
                                  x=data[incorrect, 0],
                                  y=data[incorrect, 1],
                                  text=predict[incorrect], hoverinfo='text',
                                  marker=dict(size=5, color=colorscale[digit],
                                              line=dict(width=2, color='red'),
                                              showscale=False))
        charts.extend([correct, incorrect])

    layout = plygo.Layout(title='Classification in Reduced Dimension',
                          hovermode='closest', showlegend=True,
                          legend=dict(orientation='h'),
                          xaxis=dict(title='PCA 1', zeroline=False,
                                     ticks='', showticklabels=False),
                          yaxis=dict(title='PCA 2', zeroline=False,
                                     ticks='', showticklabels=False))
    figure = plygo.Figure(data=charts, layout=layout)
    ply.offline.plot(figure, filename=filename)


def plot_pca3d_classification(classifier, pca, digits, resolution=20, filename='temp.html'):
    data = pca.transform(digits.data)
    x_min, x_max = data[:, 0].min(), data[:, 0].max()
    xs = np.linspace(x_min, x_max, resolution)
    y_min, y_max = data[:, 1].min(), data[:, 1].max()
    ys = np.linspace(y_min, y_max, resolution)
    z_min, z_max = data[:, 2].min(), data[:, 2].max()
    zs = np.linspace(z_min, z_max, resolution)
    x, y, z = np.meshgrid(xs, ys, zs)
    xyz = np.c_[x.ravel(), y.ravel(), z.ravel()]

    xyz_boundary = pca.inverse_transform(xyz)
    boundary = classifier.predict(xyz_boundary)
    predict = classifier.predict(digits.data)

    charts = []
    for digit in np.unique(digits.target):
        decision = np.where(boundary == digit)[0]
        points = np.where(digits.target == digit)[0]
        mesh = plygo.Mesh3d(name='{}'.format(digit),
                            legendgroup='digit{}'.format(digit),
                            showlegend=False,
                            x=xyz[decision, 0],
                            y=xyz[decision, 1],
                            z=xyz[decision, 2],
                            flatshading=True,
                            alphahull=0, hoverinfo='name',
                            opacity=0.2, color=colorscale[digit])
        scatter = plygo.Scatter3d(name='{}'.format(digit), mode='markers',
                                  legendgroup='digit{}'.format(digit),
                                  showlegend=True,
                                  x=data[points, 0],
                                  y=data[points, 1],
                                  z=data[points, 2],
                                  text=predict[points[0]], hoverinfo='text',
                                  marker=dict(size=7, color=colorscale[digit],
                                              line=dict(width=1, color='black')))

        charts.extend([mesh, scatter])

    layout = plygo.Layout(title='Classification in Reduced Dimension',
                          hovermode='closest', showlegend=True,
                          legend=dict(orientation='h'),
                          scene=dict(xaxis=dict(title='PCA 1', zeroline=False,
                                                ticks='', showticklabels=False),
                                     yaxis=dict(title='PCA 2', zeroline=False,
                                                ticks='', showticklabels=False),
                                     zaxis=dict(title='PCA 3', zeroline=False,
                                                ticks='', showticklabels=False)))
    figure = plygo.Figure(data=charts, layout=layout)
    ply.offline.plot(figure, filename=filename)
