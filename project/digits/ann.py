import unittest
import numpy as np
import scipy.optimize as spop
from sklearn.decomposition import PCA
from sklearn.base import BaseEstimator
from sklearn.grid_search import GridSearchCV
from sklearn.datasets import load_digits
from sklearn.learning_curve import learning_curve, validation_curve
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.cross_validation import train_test_split, cross_val_predict

from plot import plot_digits, plot_confusion_matrix, plot_learning_curve, plot_validation_curve, \
                 plot_pca2d_classification, plot_pca3d_classification


class ANN(BaseEstimator):

    def __init__(self, n_input, n_hidden, n_output, rlambda=1):
        self.n_input = n_input
        self.n_hidden = n_hidden
        self.n_output = n_output
        self.rlambda = rlambda
        self.theta1 = None
        self.theta2 = None

    def get_params(self, deep=True):
        """Return parameters of the neural network. Must match __init__()"""
        return {'n_input': self.n_input,
                'n_hidden': self.n_hidden,
                'n_output': self.n_output,
                'rlambda': self.rlambda}

    def set_params(self, **params):
        """Set parameters of the neural network. Must match __init__()."""
        if 'n_input' in params:
            self.n_input = params['n_input']
        if 'n_hidden' in params:
            self.n_hidden = params['n_hidden']
        if 'n_output' in params:
            self.n_output = params['n_output']
        if 'rlambda' in params:
            self.rlambda = params['rlambda']
        self.theta1 = None
        self.theta2 = None

    def fit(self, x, y):
        """Train a neural network by using "TNC", a version of Newton's method
         used for minimization.
         """
        init_theta1 = self._rand_initialize_weights(self.n_input, self.n_hidden)
        init_theta2 = self._rand_initialize_weights(self.n_hidden, self.n_output)
        init_theta = np.concatenate([init_theta1.ravel(), init_theta2.ravel()])

        optmin = spop.minimize(fun=self._cost, x0=init_theta, args=(x, y),
                              method='TNC', jac=True, options={'gtol': 1e-2})
        theta = optmin.x

        split = self.n_hidden * (self.n_input + 1)
        self.theta1 = theta[:split].reshape([self.n_hidden, self.n_input + 1])
        self.theta2 = theta[split:].reshape([self.n_output, self.n_hidden + 1])

    def predict(self, x):
        """Predict the label of an input from the trained weights of a neural
         theta1 and theta2.
         """
        m, n = x.shape

        a1 = np.hstack([np.ones([m, 1]), x])

        z2 = a1 @ self.theta1.T
        a2 = np.hstack([np.ones([m, 1]), self.sigmoid(z2)])

        z3 = a2 @ self.theta2.T
        a3 = self.sigmoid(z3)

        p = np.argmax(a3, axis=1)
        return p

    def fit_input(self, y_desired, xlambda=0.01):
        """Implements back-propagation of each possible canonical output (i.e.
         network is 100% sure that the result is a particular digit).
         """
        x = np.zeros([y_desired.shape[0], self.n_input])
        for i, y in enumerate(y_desired):
            init_x = np.random.random_integers(0, 16, [1, self.n_input])
            optmin = spop.minimize(fun=self._cost_input, x0=init_x, args=(y, xlambda),
                                   method='TNC', jac=True, options={'gtol': 1e-4})
            x[i, :] = optmin.x
        return x

    def _cost(self, theta, x, y):
        """Implements the neural network cost function for a two layer neural
         network which performs classification. Computes the cost and gradient of
         the neural network. The parameters for the neural network are "unrolled"
         into the vector theta and need to be converted back into the weight
         matrices.
         """
        m, n = x.shape
        split = self.n_hidden * (self.n_input + 1)
        theta1 = theta[:split].reshape([self.n_hidden, self.n_input + 1])
        theta2 = theta[split:].reshape([self.n_output, self.n_hidden + 1])

        # Feed-forward to calculate cost
        a1 = np.hstack([np.ones([m, 1]), x])
        z2 = a1 @ theta1.T
        a2 = np.hstack([np.ones([m, 1]), self.sigmoid(z2)])
        z3 = a2 @ theta2.T
        a3 = self.sigmoid(z3)

        h = a3
        yy = np.zeros(h.shape)
        yy[np.arange(m), y] = 1

        reg_theta1 = np.hstack([np.zeros([self.n_hidden, 1]), theta1[:, 1:]])
        reg_theta2 = np.hstack([np.zeros([self.n_output, 1]), theta2[:, 1:]])

        J = (- (yy * np.log(h) + (1 - yy) * np.log(1 - h)).sum()
             + ((reg_theta1 ** 2).sum() + (reg_theta2 ** 2).sum()) * self.rlambda / 2) / m

        # Back-propagate to calculate gradient
        delta3 = a3 - yy
        grad_theta2 = (delta3.T @ a2 + reg_theta2 * self.rlambda) / m

        delta2 = delta3 @ theta2
        delta2 = delta2[:, 1:] * self.sigmoid_gradient(z2)
        grad_theta1 = (delta2.T @ a1 + reg_theta1 * self.rlambda) / m

        grad_theta = np.concatenate([grad_theta1.ravel(), grad_theta2.ravel()])

        return float(J), grad_theta

    def _cost_input(self, x, y_desired, xlambda=1):
        """Calculate the cost of having x as input with a the y_desired the target output.
         Back-propagation goes all the way to the input layer to provide gradients of how
         to update inputs to achieve the desired output y_desired. Cost function is the
         norm of a difference between actual outptu and desired output.
         Regularization parameter xlambda ensures that the network doesn't hallucinate
         ideal, but suprious inputs that never appeared in the training set. This regularization
         parameter is independet from the network regularization parameter rlambda.
         """
        x = np.atleast_2d(x)
        m, n = x.shape
        # Feed-forward the provided input to calculate the cost function.
        a1 = np.hstack([np.ones([m, 1]), x])
        z2 = a1 @ self.theta1.T
        a2 = np.hstack([np.ones([m, 1]), self.sigmoid(z2)])
        z3 = a2 @ self.theta2.T
        a3 = self.sigmoid(z3)

        yy = a3
        yy_desired = np.zeros(yy.shape)
        yy_desired[np.arange(m), y_desired] = 1

        J = (((yy_desired - yy)**2).sum() + (x**2).sum() * xlambda) / 2 / self.n_input

        # Back-propagate all the way to the input layer to calculate gradient of input
        delta3 = (a3 - yy_desired) * self.sigmoid_gradient(z3)
        delta2 = delta3 @ self.theta2
        delta2 = delta2[:, 1:] * self.sigmoid_gradient(z2)
        delta1 = delta2 @ self.theta1
        delta1 = delta1[:, 1:]

        grad_x = (delta1 + x * xlambda) / self.n_input

        return float(J), grad_x

    def _rand_initialize_weights(self, l_in, l_out):
        """Randomly initialize the weights of a layer with l_in incoming
         connections and l_out outgoing connections

         Note that w should be set to a matrix of size(L_out, 1 + L_in) as the
         column row of w handles the "bias" terms
         """
        eps = np.sqrt(6) / np.sqrt(l_in + l_out)
        w = np.random.uniform(-eps, +eps, [l_out, l_in + 1])
        return w

    @staticmethod
    def sigmoid(z):
        """Compute sigmoid/logistic function of z. This should work regardless
         if z is a matrix or a vector. In particular, if z is a vector or matrix,
         you should return the gradient for each element."""
        g = 1 / (1 + np.exp(-z))
        return g

    @staticmethod
    def sigmoid_gradient(z):
        """Computes the gradient of the sigmoid function evaluated at z. This
         should work regardless if z is a matrix or a vector. In particular,
         if z is a vector or matrix, you should return the gradient for each element.
         """
        sig = ANN.sigmoid(z)
        g = sig * (1 - sig)
        return g


class TestANN(unittest.TestCase):
    def test_cost_gradient(self):
        np.random.seed(11)

        # Crate a small ANN with pseudo-random weights
        ann = ANN(10, 5, 3)
        ann.theta1 = ann._rand_initialize_weights(ann.n_input, ann.n_hidden)
        ann.theta2 = ann._rand_initialize_weights(ann.n_hidden, ann.n_output)

        # Generate random point where to calculate derivative
        theta1 = ann._rand_initialize_weights(ann.n_input, ann.n_hidden)
        theta2 = ann._rand_initialize_weights(ann.n_hidden, ann.n_output)
        theta = np.concatenate([theta1.ravel(), theta2.ravel()])

        # Reusing generate random samples x and targets y
        m = 20
        x = np.random.random_integers(0, 16, [m, ann.n_input])
        y = np.random.random_integers(0, ann.n_output-1, [m,])

        # Numerically verify non-regularized gradient
        ann.rlambda = 0
        grad = ann._cost(theta, x, y)[1]
        numgrad = self.compute_numerical_gradient(func=lambda t, x, y: ann._cost(t, x, y)[0],
                                                  x0=theta, args=(x, y), epsilon=1e-4)
        error = np.linalg.norm(numgrad - grad) / np.linalg.norm(numgrad + grad)
        self.assertLess(error, 1e-7, 'Non-regularized analytical and numerical gradient differ more than by 1e-7.')

        # Numerically verify regularized gradient
        ann.rlambda = 1
        grad = ann._cost(theta, x, y)[1]
        numgrad = self.compute_numerical_gradient(func=lambda t, x, y: ann._cost(t, x, y)[0],
                                                  x0=theta, args=(x, y), epsilon=1e-4)
        error = np.linalg.norm(numgrad - grad) / np.linalg.norm(numgrad + grad)
        self.assertLess(error, 1e-7, 'Regularized analytical and numerical gradient differ more than by 1e-7.')


    def test_cost_input_gradient(self):
        np.random.seed(11)

        # Crate a small ANN with pseudo-random weights
        ann = ANN(10, 5, 3, 1)
        ann.theta1 = ann._rand_initialize_weights(ann.n_input, ann.n_hidden)
        ann.theta2 = ann._rand_initialize_weights(ann.n_hidden, ann.n_output)

        # Reusing generate random samples x and targets y
        m = 1
        x = np.random.random_integers(0, 16, [m, ann.n_input])
        y = np.random.random_integers(0, ann.n_output-1, [m,])

        # Numerically verify non-regularized gradient
        xlambda = 0
        grad = ann._cost_input(x, y, xlambda)[1]
        numgrad = self.compute_numerical_gradient(func=lambda x, y, l: ann._cost_input(x, y, l)[0],
                                                  x0=x, args=(y, xlambda), epsilon=1e-3)
        error = np.linalg.norm(numgrad - grad) / np.linalg.norm(numgrad + grad)
        self.assertLess(error, 1e-7, 'Non-regularized analytical and numerical gradient differ more than by 1e-7.')

        # Numerically verify regularized gradient
        xlambda = 1
        grad = ann._cost_input(x, y)[1]
        numgrad = self.compute_numerical_gradient(func=lambda x, y, l: ann._cost_input(x, y, l)[0],
                                                  x0=x, args=(y, xlambda), epsilon=1e-4)

        error = np.linalg.norm(numgrad - grad) / np.linalg.norm(numgrad + grad)
        self.assertLess(error, 1e-7, 'Regularized analytical and numerical gradient differ more than by 1e-7.')

    @staticmethod
    def compute_numerical_gradient(func, x0, args, epsilon=1e-4):
        """Computes the numerical gradient of the function J around theta.
         Calling y = func(theta, *args) should return the function value at theta.

         The following code implements numerical gradient checking, and returns
         the numerical gradient. It sets numgrad[i] to (a numerical
         approximation of) the partial derivative of func with respect to the
         i-th input argument, evaluated at theta. (i.e., numgrad[i] should
         be the (approximately) the partial derivative of J with respect to
         theta[i].)
         """
        x0 = x0.ravel()
        numgrad = np.zeros(x0.size)
        perturb = np.zeros(x0.size)

        for i in range(x0.size):
            # Set perturbation vector
            perturb[i] = epsilon
            loss1 = func(x0 - perturb, *args)
            loss2 = func(x0 + perturb, *args)

            # Compute Numerical Gradient
            numgrad[i] = (loss2 - loss1) / (2 * epsilon)
            perturb[i] = 0

        return numgrad


if __name__ == '__main__':

    # ====================== Load and Visualize Data ==============================

    print('Loading and Visualizing Data...')

    # Load digits dataset
    digits = load_digits()
    x_train, x_test, y_train, y_test = train_test_split(digits.data, digits.target,
                                                        test_size=0.25, random_state=1)

    # ====================== Train Artificial Neural Network ======================

    print('Training Artificial Neural Network (this can take a minute)...')

    # Neural network parameters
    n_input = 64  # Input images of digits are 8x8
    n_hidden = 20  # Hidden units
    n_output = 10  # Labels 0 to 9
    rlambda = 1  # Regularization parameter

    # Train NN and cross validate
    n_hiddens = [1, 5, 10, 20, 25, 30, 40, 50, 60]
    rlambdas = [1e-3, 1e-2, 1e-1, 1e0, 1e+1, 1e+2, 1e+3, 1e+4]
    param_grid = [{'n_hidden': n_hiddens, 'rlambda': rlambdas}]

    search = GridSearchCV(ANN(n_input, n_hidden, n_output, rlambda),
                          param_grid=param_grid, scoring='accuracy', cv=2,
                          refit=False, verbose=1, n_jobs=4)
    search.fit(x_train, y_train)
    search.best_params_.update(n_output=n_output, n_hidden=25, n_input=n_input)
    print("\nBest ANN parameters: {}\n".format(search.best_params_))

    # ====================== Plot NN Training Metrics =============================

    print('Displaying ANN Training Metrics...')

    # Retrain with optimal values from previous step
    ann = ANN(**search.best_params_)
    ann.fit(x_train, y_train)
    pred = cross_val_predict(ann, digits.data, digits.target, cv=5)

    # Print classification report
    report = classification_report(digits.target, pred)
    print("Classification report: \n{}".format(report))

    # Display mis-classified digits from the entire dataset
    misclassified = np.where(pred != digits.target)
    plot_digits(digits.data[misclassified[0], :], pred[misclassified[0]],
                digits.target[misclassified[0]], title='Misclassified Digits',
                filename='ann/misclassified_digits.html')

    # Calculate confusion matrix
    confusion_mat = confusion_matrix(digits.target, pred).astype('float')
    confusion_mat = confusion_mat / confusion_mat.sum(axis=1)[:, np.newaxis] * 100.0
    plot_confusion_matrix(confusion_mat, digits,
                          filename='ann/confusion_matrix.html')

    # Plot learning curve
    size, train, test = learning_curve(ANN(**search.best_params_), digits.data, digits.target,
                                       cv=5, scoring='accuracy', verbose=1, n_jobs=4,
                                       train_sizes=np.linspace(0.1, 1.0, 10))
    plot_learning_curve(size, train, test,
                        filename='ann/learning_curve.html')

    # Plot a validation curve for number of hidden layers
    train, test = validation_curve(ANN(**search.best_params_), digits.data, digits.target,
                                   cv=5, scoring='accuracy', verbose=1, n_jobs=4,
                                   param_name='n_hidden', param_range=n_hiddens)
    plot_validation_curve(n_hiddens, train, test, xtitle='ANN Number of Hidden Units',
                          filename='ann/validation_curve_nhidden.html')

    # Plot a validation curve for regularization parameter lambda
    train, test = validation_curve(ANN(**search.best_params_), digits.data, digits.target,
                                   cv=5, scoring='accuracy', verbose=1, n_jobs=4,
                                   param_name='rlambda', param_range=rlambdas)
    plot_validation_curve(rlambdas, train, test, xtitle='ANN Regularization Parameter lambda',
                          filename='ann/validation_curve_lambda.html')

    # ====================== Plot PCA Reduced Classification ======================

    print('Performing PCA Decomposition...')

    # Decompose dataset into two dimension and plot classification map
    pca = PCA(n_components=2)
    pca.fit(digits.data)
    plot_pca2d_classification(ann, pca, digits, 150,
                              filename='ann/pca2d_classification.html')

    # Decompose dataset into three dimension and plot classification volume
    pca = PCA(n_components=3)
    pca.fit(digits.data)
    plot_pca3d_classification(ann, pca, digits, 25,
                              filename='ann/pca3d_classification.html')

    # ====================== Visualize ANN Canonical Input ========================

    print('Visualizing ANN...')

    # Back-propagate output corresponding to each digit and display the image
    y_canonical = np.arange(n_output)
    x_canonical = ann.fit_input(y_canonical, xlambda=0.01)
    plot_digits(x_canonical, title="Visualization of Canonical Inputs",
                filename='ann/canonical_digits.html')