import numpy as np
from sklearn.svm import SVC
from sklearn.decomposition import PCA
from sklearn.grid_search import GridSearchCV
from sklearn.datasets import load_digits
from sklearn.learning_curve import learning_curve, validation_curve
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.cross_validation import train_test_split, cross_val_predict

from plot import plot_digits, plot_confusion_matrix, plot_learning_curve, plot_validation_curve, \
                 plot_pca2d_classification, plot_pca3d_classification


if __name__ == '__main__':

    # ====================== Load and Visualize Data ==============================

    print('Loading Digits Dataset...')

    # Load digits dataset
    digits = load_digits()
    x_train, x_test, y_train, y_test = train_test_split(digits.data, digits.target,
                                                        test_size=0.25, random_state=1)

    # ====================== Train Support Vector Classifier ======================

    print('Training Support Vector Classifier (this can take a minute)...')

    # Train NN and cross validate
    Cs = [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0, 1e+1, 1e+2]
    gammas = [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0, 1e+1, 1e+2]
    param_grid = [{'kernel': ['linear', 'rbf'], 'C': Cs, 'gamma': gammas}]

    search = GridSearchCV(SVC(), param_grid=param_grid, scoring='accuracy', cv=2,
                          refit=False, verbose=1, n_jobs=4)
    search.fit(x_train, y_train)
    print("\nBest SVC parameters: {}\n".format(search.best_params_))

    # ====================== Plot SVM Training Metrics ============================

    print('Displaying SVC Training Metrics...')

    # Retrain with optimal values from previous step
    svc = SVC(**search.best_params_)
    svc.fit(x_train, y_train)
    pred = cross_val_predict(svc, digits.data, digits.target, cv=5)

    # Print classification report
    report = classification_report(digits.target, pred)
    print("Classification report: \n{}".format(report))

    # Display mis-classified digits from the entire dataset
    misclassified = np.where(pred != digits.target)
    plot_digits(digits.data[misclassified[0], :], pred[misclassified[0]],
                digits.target[misclassified[0]], title='Misclassified Digits',
                filename='svc/misclassified_digits.html')

    # Calculate confusion matrix
    confusion_mat = confusion_matrix(digits.target, pred).astype('float')
    confusion_mat = confusion_mat / confusion_mat.sum(axis=1)[:, np.newaxis] * 100.0
    plot_confusion_matrix(confusion_mat, digits,
                          filename='svc/confusion_matrix.html')

    # Plot learning curve
    size, train, test = learning_curve(SVC(**search.best_params_), digits.data, digits.target,
                                       cv=5, scoring='accuracy', verbose=1, n_jobs=4,
                                       train_sizes=np.linspace(0.1, 1.0, 10))
    plot_learning_curve(size, train, test,
                        filename='svc/learning_curve.html')

    # Plot a validation curve for regularization parameter C
    train, test = validation_curve(SVC(**search.best_params_), digits.data, digits.target,
                                   cv=5, scoring='accuracy', verbose=1, n_jobs=4,
                                   param_name='C', param_range=Cs)
    plot_validation_curve(Cs, train, test, xtitle='SVC Regularization Parameter C',
                          filename='svc/validation_curve_c.html')

    # Plot a validation curve for broadcast parameter gamma
    train, test = validation_curve(SVC(**search.best_params_), digits.data, digits.target,
                                   cv=5, scoring='accuracy', verbose=1, n_jobs=4,
                                   param_name='gamma', param_range=gammas)
    plot_validation_curve(gammas, train, test, xtitle='SVC Kernel Broadcast Parameter gamma',
                          filename='svc/validation_curve_gamma.html')

    # ====================== Plot PCA Reduced Classification ======================

    print('Performing PCA Decomposition...')

    # Decompose dataset into two dimension and plot classification map
    pca = PCA(n_components=2)
    pca.fit(digits.data)
    plot_pca2d_classification(svc, pca, digits, 150,
                              filename='svc/pca2d_classification.html')

    # Decompose dataset into three dimension and plot classification volume
    pca = PCA(n_components=3)
    pca.fit(digits.data)
    plot_pca3d_classification(svc, pca, digits, 25,
                              filename='svc/pca3d_classification.html')
