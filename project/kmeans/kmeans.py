import numpy as np
from scipy.misc import imread
from sklearn.cluster import KMeans
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler
import plotly as ply
import plotly.graph_objs as plygo


# Load and normalize the image
img = imread('../../coursera/ex7/bird_small.png', mode='RGB').astype('float')
img = img.reshape([img.shape[0] * img.shape[1], 3])

# Setup and fit the normalization-clustering pipeline
scaler = MinMaxScaler(copy=True)
kmeans = KMeans(16, n_init=20, init='random')
pipe = Pipeline(steps=[['scale', scaler], ['clustering', kmeans]])
pipe.fit(img)

# Classify each pixel for plotting
clusters = pipe.predict(img)
centroids = scaler.inverse_transform(kmeans.cluster_centers_)
colorscale = []
for i, centroid in enumerate(centroids):
    color = 'rgb({c[0]},{c[1]},{c[2]})'.format(c=centroid.astype(np.uint8))
    colorscale.append([i / (len(centroids) - 1), color])

# Plot points in 3D scatter chart
pixels = plygo.Scatter3d(name='Image Pixels',
                         x=img[:, 0], y=img[:, 1], z=img[:, 2],
                         marker=dict(color=clusters, colorscale=colorscale,
                                     cmin=clusters.min(), cmax=clusters.max()),
                         mode='markers')
centroids = plygo.Scatter3d(name='Cluster Centers',
                            x=centroids[:, 0], y=centroids[:, 1], z=centroids[:, 2],
                            marker=dict(color='black', line=dict(width=5), symbol='cross'),
                            mode='markers')
layout = plygo.Layout(title='K-Means Clustering Image Compression',
                      scene=dict(xaxis=dict(title='Red'),
                                 yaxis=dict(title='Green'),
                                 zaxis=dict(title='Blue')),
                      hovermode='closest', showlegend=True)
figure = plygo.Figure(data=[pixels, centroids], layout=layout)
ply.offline.plot(figure, filename='kmeans.html')
